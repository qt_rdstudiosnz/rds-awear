/* eslint-disable no-useless-escape */
// eslint-disable-next-line no-unused-vars
var isFrontEndBuilder = isFrontEndBuilderCheck();
var cart = null;
var preventingDoubleClick = [];
var preventingDeleteDoubleClick = [];

var autocomplete;
var addressSearchField;
var address1Field;
var address2Field;
// eslint-disable-next-line no-unused-vars
var postalField;
var cityField;
var stateField;
var postCodeField;
var countryField;
var countryDiv;
var unitNumber;

var matched, browser;
var topUpValConst = 0;
var initialiseStripe;
var ajaxData = {};

var isChargingForDelivery = false;

// eslint-disable-next-line no-unused-vars
var disableClickFlag = false;

var handleErrors = function handleErrors( response ) {
	if ( ! response.ok ) {
		throw Error( response.statusText );
	}
	return response;
};

var processPayment = function processPayment( method ) {
	var r, $data;

	if ( 'token' == method ) {
		ajaxData = {};
		ajaxData.cart = JSON.stringify( window.cart );
		ajaxData.shippingMethod = localStorage.getItem( 'shippingMethod' );

		if ( isChargingForDelivery ) {
			ajaxData.shippingTotal = 10;
		}

		ajaxData.paymentMethod = 'token';
		ajaxData.action = 'process_token_payment';

		jQuery.ajax({
			type: 'post',
			dataType: 'json',
			url: window.awear.ajax_url,
			data: ajaxData,
			success: function( response ) {
				console.log( response );
				r = response;

				$data = JSON.parse( r.data );

				if ( $data.success ) {
					clearStorage();
					setTimeout( function() {
						location.href = $data.permalink;
					}, 1000 );
				} else {
					alert( $data.error );
					location.href = $data.permalink;
				}

				jQuery( '.blocker' ).css( 'display', 'none' );

			},
			error: function( jqXHR, textStatus, errorThrown ) {
				jQuery( '.blocker' ).css( 'display', 'none' );

				console.log( jqXHR );
				console.log( textStatus );
				console.log( errorThrown );
			}
		});
	} else if ( 'card' == method ) {

		// Card Payment

	}
};

var updateUserBalance = function updateUserBalance( val ) {
	document.querySelector( '#user-token-balance' );

	let cardButton = document.querySelector( '.card-payment' );
	let tokenButton = document.querySelector( '.token-payment' );

	const data = new FormData();
	data.append( 'credentials', 'same-origin' );
	data.append( 'action', 'updateUserBalance' );
	data.append( 'addedTokens', val );
	data.append( 'awearUser', window.awear.awearUser );

	fetch( window.awear.ajax_url, {
		method: 'POST',
		body: data
	})
		.then( handleErrors )
		.then( function( result ) {
			return result.json();
		}).then( function( data ) {
			console.log( data );
			if ( null != cardButton ) {
				animateDisplay( cardButton, 'hide', true );
			}
			if ( null != tokenButton ) {
				animateDisplay( tokenButton, 'show', true );
			}
			processPayment( 'token' );
		}).catch( function( err ) {
			console.log( 'Fetch Error' );
			console.log( err );
		});
};

// eslint-disable-next-line no-unused-vars
var checkPaymentIntent = function checkPaymentIntent() {

	//if ( topUpValConst != jQuery( '.top-up-value' ).attr( 'data-val' ) ) {

	// SET PAYMENT TOTAL

	if ( 0 > jQuery( '#token-input' ).val() ) {
		topUpValConst = jQuery( '.top-up-value' ).attr( 'data-val' );
	}

	const data = new FormData();
	data.append( 'credentials', 'same-origin' );
	data.append( 'action', 'setTopUpValue' );
	data.append( 'topUpVal', jQuery( '.top-up-value' ).attr( 'data-val' ) );
	data.append( 'tokenCount', jQuery( '.token-input' ).val() );

	fetch( window.awear.ajax_url, {
		method: 'POST',
		body: data
	}).then( function( result ) {
		return result.json();
	}).then( function( data ) {
		console.log( data );
	});

	//}

};

var animateDisplay = function animateDisplay( element, type = 'hide', delay = true ) {

	if ( delay ) {
		element.style.transition = '600ms';
	}

	switch ( type ) {
	case 'show':
		setShowHide( element, type, delay );
		element.style.opacity = 1;
		break;

	case 'hide':
	default:
		element.style.opacity = 0;
		setShowHide( element, type, delay );
		break;
	}

};

var setShowHide = function setShowHide( element, type = 'hide', delay = true ) {
	switch ( type ) {
	case 'show':

		if ( ! delay ) {
			element.style.display = 'block';
		} else {
			setTimeout( function() {
				element.style.display = 'block';
			}, 600 );
		}
		break;

	case 'hide':
	default:

		if ( ! delay ) {
			element.style.display = 'none';
		} else {
			setTimeout( function() {
				element.style.display = 'none';
			}, 600 );
		}

		break;
	}
};

var payByCard = function payByCard() {

	let changeTokenButton = document.getElementById( 'change-token-amount' );

	changeTokenButton.addEventListener( 'click', changeTokenAmount );
	changeTokenButton.addEventListener( 'touchend', changeTokenAmount );

	hidePaymentSetUpForm();
	showCCForm();
};

var changeTokenAmount = function changeTokenAmount() {
	hideCCForm();
	setTimeout( showPaymentSetUpForm, 600 );
};

var hidePaymentSetUpForm = function hidePaymentSetUpForm() {
	let paymentSetUpWrap = document.getElementById( 'payment-setup' );

	// paymentSetUpWrap.style.opacity = 0;
	animateDisplay( paymentSetUpWrap, 'hide', true );
};

var showPaymentSetUpForm = function showPaymentSetUpForm() {
	let paymentSetUpWrap = document.getElementById( 'payment-setup' );
	animateDisplay( paymentSetUpWrap, 'show' );

	// paymentSetUpWrap.style.opacity = 1;
};

var showCCForm = function showCCForm() {
	let payForm = document.getElementById( 'form-wrapper' );
	animateDisplay( payForm, 'show' );

	// payForm.style.opacity = 1;
	initialiseStripe();
};

var hideCCForm = function hideCCForm() {
	let payForm = document.getElementById( 'form-wrapper' );

	// payForm.style.opacity = 0;
	animateDisplay( payForm, 'hide', true );

};

var tokenAmountChange = function tokenAmountChange() {

	let min = jQuery( this ).data( 'min' );
	let val = jQuery( '.token-input' ).val();

	if ( val < min ) {
		val = min;
	}

	jQuery( '.token-input' ).val( val );

};

var sqMinus = function minus() {

	// e.preventDefault();
	let val = jQuery( '.token-input' ).val();
	let min = jQuery( this ).data( 'min' );
	let calcVal = jQuery( '.top-up-value' ).data( 'calc' );

	if ( val > min ) {
		val = parseInt( val ) - 1;
	} else {
		val = min;
	}
	jQuery( '.token-input' ).val( val );

	if ( isChargingForDelivery ) {

		let deliveryValue = document.getElementById( 'delivery-value' );
		let delVal = deliveryValue.getAttribute( 'data-val' );
		let tempVal = ( parseInt( val ) * parseInt( calcVal ) ) + parseInt( delVal );

		jQuery( '.top-up-value' ).html( '$' + parseInt( tempVal ) );
		jQuery( '.top-up-value' ).attr( 'data-val', parseInt( tempVal )  );

	} else {

		jQuery( '.top-up-value' ).html( '$' + parseInt( val ) * parseInt( calcVal ) );
		jQuery( '.top-up-value' ).attr( 'data-val', parseInt( val ) * parseInt( calcVal ) );

	}

	hideCCForm();

};

var sqPlus = function plus() {

	// e.preventDefault();
	let val = jQuery( '.token-input' ).val();
	let calcVal = jQuery( '.top-up-value' ).data( 'calc' );

	val = parseInt( val ) + 1;
	jQuery( '.token-input' ).val( val );

	if ( isChargingForDelivery ) {

		let deliveryValue = document.getElementById( 'delivery-value' );
		let delVal = deliveryValue.getAttribute( 'data-val' );
		let tempVal = ( parseInt( val ) * parseInt( calcVal ) ) + parseInt( delVal );

		jQuery( '.top-up-value' ).html( '$' + parseInt( tempVal ) );
		jQuery( '.top-up-value' ).attr( 'data-val', parseInt( tempVal )  );

	} else {

		jQuery( '.top-up-value' ).html( '$' + parseInt( val ) * parseInt( calcVal ) );
		jQuery( '.top-up-value' ).attr( 'data-val', parseInt( val ) * parseInt( calcVal ) );

	}

	hideCCForm();

};

jQuery.uaMatch = function( ua ) {
	var match =
		/(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		/(msie) ([\w.]+)/.exec( ua ) ||
		( 0 > ua.indexOf( 'compatible' ) &&
			/(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ) ||
		[];

	ua = ua.toLowerCase();

	return {
		browser: match[1] || '',
		version: match[2] || '0'
	};
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
	browser[matched.browser] = true;
	browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
	browser.webkit = true;
} else if ( browser.webkit ) {
	browser.safari = true;
}

jQuery.browser = browser;

// eslint-disable-next-line no-unused-vars
function initAutocomplete() {
	if ( ! isFrontEndBuilder ) {

		addressSearchField = ( jQuery( '#ff_3_address_search' ).length ) ? document.querySelector( '#ff_3_address_search' ) : document.querySelector( '#acf-field_6076bed2b0148' );
		unitNumber = ( jQuery( '#ff_3_Unit_Number' ).length ) ? document.querySelector( '#ff_3_Unit_Number' ) : document.querySelector( '#acf-field_6077814ddfdfe' );

		address1Field = ( jQuery( '#ff_3_Address_Line_1' ).length ) ? document.querySelector( '#ff_3_Address_Line_1' ) : document.querySelector( '#acf-field_6074ca0d541d3' );
		address2Field = ( jQuery( '#ff_3_Address_Line_2' ).length ) ? document.querySelector( '#ff_3_Address_Line_2' ) : document.querySelector( '#acf-field_6074ca11541d4' );
		// eslint-disable-next-line no-unused-vars
		cityField = ( jQuery( '#ff_3_City' ).length ) ? document.querySelector( '#ff_3_City' ) : document.querySelector( '#acf-field_6074ca22541d5' );
		// eslint-disable-next-line no-undef
		// eslint-disable-next-line no-unused-vars
		stateField = ( jQuery( '#ff_3_State' ).length ) ? document.querySelector( '#ff_3_State' ) : document.querySelector( '#acf-field_6074ca31541d6' );
		postCodeField = ( jQuery( '#ff_3_Post_Code' ).length ) ? document.querySelector( '#ff_3_Post_Code' ) : document.querySelector( '#acf-field_6074ca35541d7' );
		// eslint-disable-next-line no-unused-vars
		countryDiv = ( jQuery( '.choices__item' ).length ) ? document.querySelector( '.choices__item' ) : '';
		// eslint-disable-next-line no-unused-vars
		countryField = ( jQuery( '#ff_3_country' ).length ) ? document.querySelector( '#ff_3_country' ) : document.querySelector( '#acf-field_6074ca4c541d8' );

		// Create the autocomplete object, restricting the search predictions to
		// addresses in the US and Canada.

		// eslint-disable-next-line no-undef
		autocomplete = new google.maps.places.Autocomplete( addressSearchField, {
			componentRestrictions: { country: [ 'nz' ] },
			fields: [ 'address_components', 'geometry' ],
			types: [ 'address' ]
		});

		addressSearchField.focus();

		// When the user selects an address from the drop-down, populate the
		// address fields in the form.
		autocomplete.addListener( 'place_changed', fillInAddress );
	}
}

function fillInAddress() {

	// Get the place details from the autocomplete object.
	const place = autocomplete.getPlace();
	let address1 = '';
	let postcode = '';

	// Get each component of the address from the place details,
	// and then fill-in the corresponding field on the form.
	// place.address_components are google.maps.GeocoderAddressComponent objects
	// which are documented at http://goo.gle/3l5i5Mr
	for ( const component of place.address_components ) {
		const componentType = component.types[0];

		let selectElement, i, L, opt;

		switch ( componentType ) {
		case 'street_number': {
			address1 = `${component.long_name} ${address1}`;
			break;
		}
		case 'subpremise': {
			unitNumber = `${component.long_name}`;

			if ( jQuery( '#ff_3_Unit_Number' ).length ) {
				document.querySelector( '#ff_3_Unit_Number' ).value = unitNumber;
			}

			if ( jQuery( '#acf-field_6077814ddfdfe' ).length ) {
				document.querySelector( '#acf-field_6077814ddfdfe' ).value = unitNumber;
			}

			break;
		}
		case 'route': {
			address1 += component.short_name;
			break;
		}

		case 'postal_code': {
			postcode = `${component.long_name}${postcode}`;
			break;
		}

		case 'postal_code_suffix': {
			postcode = `${postcode}-${component.long_name}`;
			break;
		}
		case 'locality':

			if ( jQuery( '#ff_3_City' ).length ) {
				document.querySelector( '#ff_3_City' ).value = component.long_name;
			}

			if ( jQuery( '#acf-field_6074ca22541d5' ).length ) {
				document.querySelector( '#acf-field_6074ca22541d5' ).value = component.short_name;
			}

			break;

		case 'sublocality':
		case 'political':
		case 'sublocality_level_1': {
			address2Field.value = component.long_name;
			break;
		}
		case 'administrative_area_level_1': {

			if ( jQuery( '#ff_3_State' ).length ) {
				document.querySelector( '#ff_3_State' ).value = component.short_name;
			}

			if ( jQuery( '#acf-acf-field_6074ca31541d6' ).length ) {
				document.querySelector( '#acf-field_6074ca31541d6' ).value = component.short_name;
			}
			break;

		}

		case 'country':

			if ( jQuery( '#ff_3_country' ).length ) {
				selectElement = document.querySelector( '#ff_3_country' );
				L = selectElement.options.length - 1;
				for ( i = L; 0 <= i; i-- ) {
					selectElement.remove( i );
				}
			}

			if ( jQuery( '#acf-field_6074ca4c541d8' ).length ) {
				document.querySelector( '#acf-field_6074ca4c541d8' ).value = component.long_name;
			}

			if ( jQuery( '.choices__item' ).length ) {
				opt = document.createElement( 'option' );
				opt.value = component.short_name;
				opt.innerHTML = component.long_name;
				selectElement.appendChild( opt );

				if ( jQuery( '.choices__item' ).length ) {
					document.querySelector( '.choices__item' ).innerHTML = component.long_name;
				}
			}

			break;
		}
	}
	address1Field.value = address1;
	postCodeField.value = postcode;

	// After filling the form with address components from the Autocomplete
	// prediction, set cursor focus on the second address line to encourage
	// entry of subpremise information such as apartment, unit, or floor number.
	//geoCodeMe();
	let lat = place.geometry.location.lat();
	let lng = place.geometry.location.lng();

	if ( jQuery( '[name="lat"]' ).length ) {
		document.querySelector( '[name="lat"]' ).value = lat;
	}
	if ( jQuery( '[name="lng"]' ).length ) {
		document.querySelector( '[name="lng"]' ).value = lng;
	}

	if ( jQuery( '#acf-field_60750fc7324df' ).length ) {
		document.querySelector( '#acf-field_60750fc7324df' ).value = lat;
	}
	if ( jQuery( '#acf-field_60750fcb324e0' ).length ) {
		document.querySelector( '#acf-field_60750fcb324e0' ).value = lng;
	}

	if ( jQuery( '.map' ).length ) {
		jQuery( '.map' ).attr(
			'src',
			'https://maps.googleapis.com/maps/api/staticmap?center=' +
				lat +
				'%2c%20' +
				lng +
				'&zoom=16&&markers=color:0xb97d3c|' +
				lat +
				'%2c%20' +
				lng +
				'&size=640x320&style=feature:poi|visibility:off&scale=2&key=AIzaSyBWc0uEhrQcByHtZmSAz8quYTnCuxvrhuQ'
		);
	}

	address2Field.focus();
}

// eslint-disable-next-line no-unused-vars
function geoCodeMe() {
	// eslint-disable-next-line no-undef
	const geocoder = new google.maps.Geocoder();

	let addressSearch = addressSearchField.value;
	geocoder.geocode({ address: addressSearch }, ( results, status ) => {
		if ( 'OK' === status ) {

			//console.log(results[0].geometry.location);
			console.log( results[0].geometry.location.lat() );
			console.log( results[0].geometry.location.lng() );
			document.querySelector( '[name="lat"]' ).value =
				results[0].geometry.location.lat();
			document.querySelector( '[name="lng"]' ).value =
				results[0].geometry.location.lng();

			if ( jQuery( '.map' ).length ) {
				jQuery( '.map' ).attr(
					'src',
					'https://maps.googleapis.com/maps/api/staticmap?center=' +
						results[0].geometry.location.lat() +
						'%2c%20' +
						results[0].geometry.location.lng() +
						'&zoom=16&&markers=color:0xb97d3c|' +
						results[0].geometry.location.lat() +
						'%2c%20' +
						results[0].geometry.location.lng() +
						'&size=640x320&style=feature:poi|visibility:off&scale=2&key=AIzaSyBWc0uEhrQcByHtZmSAz8quYTnCuxvrhuQ'
				);
			}

			//addressSearchField.value = "";
		} else {
			console.log(
				'Geocode was not successful for the following reason: ' + status
			);
		}
	});
}

( function() {
	'use strict';

	// eslint-disable-next-line no-unused-vars
	initialiseStripe = function() {
		// eslint-disable-next-line no-undef
		var stripe = Stripe( window.awear.pk );

		// Calls stripe.confirmCardPayment
		// If the card requires authentication Stripe shows a pop-up modal to
		// prompt the user to enter authentication details without leaving your page.

		var payWithCard = function( stripe, card, clientSecret ) {
			loading( true );
			stripe
				.confirmCardPayment( clientSecret, {

					// eslint-disable-next-line camelcase
					receipt_email: document.getElementById( 'user_email' ).value,
					// eslint-disable-next-line camelcase
					payment_method: {
						card: card
					}
				})
				.then( function( result ) {
					if ( result.error ) {

						// Show error to your customer
						showError( result.error.message );
					} else {

						// The payment succeeded!
						orderComplete( result );
					}
				});
		};

		/* ------- UI helpers ------- */
		// Shows a success message when the payment is complete

		// eslint-disable-next-line no-unused-vars
		var orderComplete = function( result ) {
			loading( false );

			console.log( result );
			let val = jQuery( '.token-input' ).val();

			updateUserBalance( val );

			//Output Message & Disable Button
			document.querySelector( '.result-message' ).classList.remove( 'hidden' );
			document.querySelector( 'button' ).disabled = true;

			//Hide Payment Form
			let form = document.getElementById( 'payment-form' );

			animateDisplay( form, 'hide', true );

			//Set new token balance
			let cartTokenElement = document.querySelector( '#user-token-balance' );
			let currentBalance = cartTokenElement.getAttribute( 'data-balance' );

			let newBalance = parseInt( currentBalance ) + parseInt( val );

			cartTokenElement.setAttribute( 'data-balance', newBalance );
			cartTokenElement.textContent = newBalance;

			// let txt = document.createTextNode( newBalance );
			// cartTokenElement.appendChild( txt );

		};

		// Show the customer the error from Stripe if their card fails to charge

		var showError = function( errorMsgText ) {
			var errorMsg = document.querySelector( '#card-error' );
			loading( false );
			errorMsg.textContent = errorMsgText;
			setTimeout( function() {
				errorMsg.textContent = '';
			}, 4000 );
		};

		// Show a spinner on payment submission

		var loading = function( isLoading ) {
			if ( isLoading ) {

				// Disable the button and show a spinner
				document.querySelector( 'button' ).disabled = true;
				document.querySelector( '#spinner' ).classList.remove( 'hidden' );
				document.querySelector( '#button-text' ).classList.add( 'hidden' );
			} else {
				document.querySelector( 'button' ).disabled = false;
				document.querySelector( '#spinner' ).classList.add( 'hidden' );
				document.querySelector( '#button-text' ).classList.remove( 'hidden' );
			}
		};


		// The items the customer wants to buy

		const data = new FormData();

		//topUpValConst = jQuery( '.top-up-value' ).attr( 'data-val' );

		if ( 0 > jQuery( '#token-input' ).val() ) {
			topUpValConst = jQuery( '.top-up-value' ).attr( 'data-val' );
		}

		let paymentTotal = parseInt( topUpValConst );
		let deliveryValue = document.getElementById( 'delivery-value' );
		let deliveryFee = deliveryValue.getAttribute( 'data-val' );

		if ( 0 < deliveryFee ) {
			paymentTotal = paymentTotal + parseInt( deliveryFee );
			data.append( 'deliveryValue', deliveryFee );
			data.append( 'deliveryCharge', 1 );

			if ( 0 == jQuery( '.token-input' ).val()  ) {
				data.append( 'description', 'Delivery' );
			} else {
				data.append( 'description', 'Account Top Up & Delivery' );
			}
		} else {
			data.append( 'description', 'Account Top Up' );
		}

		data.append( 'credentials', 'same-origin' );
		data.append( 'action', 'payment_intent' );
		data.append( 'topUpVal', paymentTotal );
		data.append( 'tokenRate', window.awear.tokenRate );
		data.append( 'isAwearArmy', window.awear.isAwearArmy );
		data.append( 'awearUser', window.awear.awearUser );
		data.append( 'tokenCount', jQuery( '.token-input' ).val() );


		// Disable the button until we have Stripe set up on the page

		document.querySelector( 'button' ).disabled = true;
		fetch( window.awear.ajax_url, {
			method: 'POST',
			body: data
		})
			.then( function( result ) {
				return result.json();
			})

			.then( function( data ) {
				var elements = stripe.elements();
				var style = {
					base: {
						color: '#3d190d',
						fontFamily: 'Lato, sans-serif',
						fontSmoothing: 'antialiased',
						fontSize: '16px',
						'::placeholder': {
							color: '#b97d3c'
						}
					},
					invalid: {
						fontFamily: 'Lato, sans-serif',
						color: '#b97d3c',
						iconColor: '#b97d3c'
					}
				};

				var card = elements.create( 'card', { style: style });

				// Stripe injects an iframe into the DOM
				card.mount( '#card-element' );
				card.on( 'change', function( event ) {

					//Check the Top Up Amount and if it has changed, push push paymentIntent
					//checkPaymentIntent();

					// Disable the Pay button if there are no card details in the Element
					document.querySelector( 'button' ).disabled = event.empty;
					document.querySelector( '#card-error' ).textContent = event.error ? event.error.message : '';
				});

				let form = document.getElementById( 'payment-form' );
				form.addEventListener( 'submit', function( event ) {
					event.preventDefault();

					// Complete payment when the submit button is clicked
					payWithCard( stripe, card, data.clientSecret );
				});
			});
	};

	jQuery( window ).on( 'scroll', function() {

		// console.log( window.scrollY );
		requestAnimationFrame( stickyNav );
	});

	function stickyNav() {
		let oT = jQuery( window ).scrollTop();

		if ( 0 < oT ) {
			jQuery( '.nav-bar' ).addClass( 'sticky' );
		} else {
			jQuery( '.nav-bar' ).removeClass( 'sticky' );
		}
	}

	if ( jQuery( '.single-item-image' ).length ) {
		jQuery( '.single-item-image' ).imageZoom({
			zoom: 200
		});
	}
}( jQuery ) );

// eslint-disable-next-line no-unused-vars
function isFrontEndBuilderCheck() {
	var attr = jQuery( 'html' ).attr( 'ng-app' );
	if ( typeof attr !== typeof undefined && false !== attr ) {
		return 'CTFrontendBuilder' == attr || 'CTFrontendBuilderUI' == attr;
	} else {
		return false;
	}
}

function willJSONParse( json ) {
	var $json;
	try {
		$json = JSON.parse( json );
	} catch ( e ) {
		return false;
	}

	return $json;
}

function clearStorage() {
	window.cart = [];
	localStorage.removeItem( 'cart' );
	localStorage.removeItem( 'cartModified' );
	localStorage.removeItem( 'shippingMethod' );
	return true;
}

( function() {

	// jQuery( 'body' ).on( 'click touchend', 'a', function( e ) {
	// 	if ( disableClickFlag ) {
	// 		e.preventDefault();
	// 	}
	// });

	// jQuery( 'body' ).on( 'click touchend', '.add-to-cart', function( e ) {
	// 	if ( disableClickFlag ) {
	// 		e.preventDefault();
	// 	}
	// });

	// jQuery( window ).scroll( function() {
	// 	disableClickFlag = true;

	// 	clearTimeout( jQuery.data( this, 'scrollTimer' ) );

	// 	jQuery.data( this, 'scrollTimer', setTimeout( function() {
	// 		disableClickFlag = false;
	// 	}, 250 ) );
	// });

	let cCart =
		null === window.awear.currentCart ||
		'null' === window.awear.currentCart ||
		'' === window.awear.currentCart ?
			[] :
			JSON.parse( window.awear.currentCart );
	cart =
		null === localStorage.getItem( 'cart' ) ||
		'null' === localStorage.getItem( 'cart' ) ||
		'' === localStorage.getItem( 'cart' ) ?
			cCart :
			JSON.parse( localStorage.getItem( 'cart' ) );
	let cartModified = localStorage.getItem( 'cartModified' );

	jQuery.extend(
		( jQuery.fn.disableTextSelect = function() {
			return this.each( function() {
				if ( jQuery.browser.mozilla ) {

					//Firefox
					jQuery( this ).css( 'MozUserSelect', 'none' );
				} else if ( jQuery.browser.msie ) {

					//IE
					jQuery( this ).bind( 'selectstart', function() {
						return false;
					});
				} else {

					//Opera, etc.
					jQuery( this ).mousedown( function() {
						return false;
					});
				}
			});
		})
	);

	// jQuery( '.sq-button' ).disableTextSelect();

	if ( null === localStorage.getItem( 'cart' ) ) {
		localStorage.setItem( 'cart', JSON.stringify( cart ) );
	}

	if ( null !== cartModified ) {
		// eslint-disable-next-line no-undef
		if ( moment().format( 'X' ) > cartModified ) {

			//custom clear storage
			clearStorage();

			ajaxData.action = 'destroySession';

			jQuery.ajax({
				type: 'post',
				dataType: 'json',
				url: window.awear.ajax_url,
				data: ajaxData,
				success: function( response ) {
					console.log( response );
					location.reload();
				},
				error: function( jqXHR, textStatus, errorThrown ) {
					console.log( jqXHR );
					console.log( textStatus );
					console.log( errorThrown );
				}
			});
		}
	} else {
		// eslint-disable-next-line no-undef
		localStorage.setItem( 'cartModified', moment().add( 30, 'm' ).format( 'X' ) );
	}

	jQuery( document ).on( 'click', '.login-now', function() {
		jQuery( '#_tab-47-422' ).click();
	});

	jQuery( '.mobile-open-bag' ).on( 'click touchend', function() {

		//oxyCloseModal(document.getElementById('modal-163-50'));
		jQuery( '#modal-163-50' ).fadeOut();
		jQuery( '#modal-163-50' ).parent().fadeOut();
		jQuery( '.open-bag' ).click();
	});

	jQuery( document ).on( 'click touchend', '.added-to-bag', function() {
		let id = jQuery( this ).attr( 'data-id' );
		if ( ! preventingDoubleClick[id]) {
			preventingDoubleClick[id] = true;
			preventingDeleteDoubleClick[id] = false;
			jQuery( '.open-bag' ).click();
		}
	});

	jQuery( document ).on( 'click touchend', '.mini-img', function() {
		let imgUrl = jQuery( this ).data( 'img' );
		if ( jQuery( '.containerZoom' ).length ) {
			jQuery( '.containerZoom' ).css( 'background-image', 'url(' + imgUrl + ')' );
		}
		if ( jQuery( '#imageZoom' ).length ) {
			jQuery( '#imageZoom' ).attr( 'src', imgUrl );
		}
		if ( jQuery( '.single-item-image' ).length ) {
			jQuery( '.single-item-image' ).attr( 'src', imgUrl );
		}
	});

	jQuery( document ).on( 'click', '.add-to-cart', function() {

		let buttenText = jQuery( this ).find( ' .btn-text-inner' );
		let buttonAnimator = jQuery( this ).find( ' .btn-ani-wrap' );

		let id = jQuery( this ).attr( 'data-id' );
		if ( ! preventingDoubleClick[id]) {
			preventingDoubleClick[id] = true;
			preventingDeleteDoubleClick[id] = false;

			buttonAnimator.fadeIn();
			buttenText.fadeOut();

			let that = jQuery( this );

			let itemName = jQuery( this ).data( 'name' );
			let slug = jQuery( this ).data( 'slug' );
			let id = jQuery( this ).data( 'id' );
			let thumb = jQuery( this ).data( 'thumb' );
			let price = jQuery( this ).data( 'price' );
			let colour = jQuery( this ).data( 'colour' );
			let size = jQuery( this ).data( 'size' );
			let nonce = jQuery( this ).data( 'nonce' );

			//CREATE CART OBJECT

			let item = {
				id: id,
				name: itemName,
				slug: slug,
				thumb: thumb,
				price: price,
				size: size,
				colour: colour
			};

			//AJAX POST DATA
			ajaxData = item;
			ajaxData.action = 'add_to_cart';
			ajaxData.nonce = nonce;

			jQuery.ajax({
				type: 'post',
				dataType: 'json',
				url: window.awear.ajax_url,
				data: ajaxData,
				success: function( response ) {
					console.log( response );

					let willParse = willJSONParse( response.data );

					//LET'S CHECK TO SEE IF WE CAN ADD THIS ITEM WAS ADDED TO THE CART
					if ( ! willParse || ! Array.isArray( willParse ) ) {

						//FAILED
						alert( response.data );
					} else {

						//MUCH SUCCESS

						if ( ! jQuery( '.empty-message' ).hasClass( 'hidden' ) ) {
							jQuery( '.empty-message' ).addClass( 'hidden' ).hide();
						}
						jQuery( '.open-bag' ).click();

						let currency =
							1 == price ?
								window.awear.currencyName :
								window.awear.currencyName + 's';

						if ( ! window.awear.isSingle ) {

							//that.closest( '.item' ).parent().fadeOut( 'slow' );

						} else if ( window.awear.isSingle ) {
							let buttonHtml = `<div class="ct-link-button button w-100 added-to-bag" data-id="${id}">ADDED TO BAG</a>`;

							that.fadeOut( 'slow', function() {
								jQuery( buttonHtml ).hide().insertAfter( this ).fadeIn( 'slow' );
							});

						}

						let html = `<div class="cart-item">
																<div class="cart-thumb">
																	<img alt="${itemName}" src="${thumb}" class="ct-image">
																	<div class="remove-from-bag" data-id="${id}" data-slug="${slug}">
																		<svg viewBox="0 0 24 28">
																				<path d="M19 15v-2c0-0.547-0.453-1-1-1h-12c-0.547 0-1 0.453-1 1v2c0 0.547 0.453 1 1 1h12c0.547 0 1-0.453 1-1zM24 14c0 6.625-5.375 12-12 12s-12-5.375-12-12 5.375-12 12-12 12 5.375 12 12z"></path>
																		</svg>
																	</div>
																</div>
																<div class="cart-item-data">
																	<div class="cart-item-title">${itemName}</div>
																	<div class="cart-item-colour mb-0">${colour}</div>
																	<div class="cart-item-colour">${size}</div>
																	<div class="cart-item-price">${price} ${currency}</div>
																</div>
															</div>`;

						jQuery( '.cart-inner' ).append( html );
						bindButtons();

						window.cart.push( item );

						cartTotal();

						localStorage.setItem( 'cart', JSON.stringify( window.cart ) );
						// eslint-disable-next-line no-undef
						localStorage.setItem(
							'cartModified',
							// eslint-disable-next-line no-undef
							moment().add( 30, 'm' ).format( 'X' )
						);
					}

					buttenText.fadeIn();
					buttonAnimator.fadeOut();

				},
				error: function( jqXHR, textStatus, errorThrown ) {
					console.log( jqXHR );
					console.log( textStatus );
					console.log( errorThrown );
				}
			});

			return false;
		}
	});

	function cartTotal() {
		let total = 0;

		// eslint-disable-next-line no-unused-vars
		window.cart.forEach( function( item, index ) {
			total = parseInt( total ) + parseInt( item.price );
		});

		jQuery( '.cart-total' ).html( total );

		const tokenTitle =
			1 === total ? window.awear.currencyName : window.awear.currencyName + 's';
		jQuery( '.cart-token' ).html( tokenTitle );

		return total;
	}

	function bindButtons() {
		jQuery( '.remove-from-bag' ).unbind( 'click' ); // To prevent double-binding click events!
		// eslint-disable-next-line no-unused-vars
		jQuery( '.remove-from-bag' ).on( 'click touchend', function( e ) {
			let id = jQuery( this ).attr( 'data-id' );

			if ( ! preventingDeleteDoubleClick[id]) {
				preventingDeleteDoubleClick[id] = true;
				preventingDoubleClick[id] = false;

				id = jQuery( this ).data( 'id' );
				window.cart = window.cart.filter( function( item ) {
					return item.id != id;
				});

				jQuery( this ).closest( '.cart-item' ).fadeOut( 'slow' );

				cartTotal();

				localStorage.setItem( 'cart', JSON.stringify( window.cart ) );

				// eslint-disable-next-line no-undef
				localStorage.setItem( 'cartModified', moment().add( 30, 'm' ).format( 'X' ) );

				ajaxData = {};
				ajaxData.id = id;
				ajaxData.action = 'remove_from_cart';

				jQuery.ajax({
					type: 'post',
					dataType: 'json',
					url: window.awear.ajax_url,
					data: ajaxData,
					success: function( response ) {
						console.log( response );
					},
					error: function( jqXHR, textStatus, errorThrown ) {
						console.log( jqXHR );
						console.log( textStatus );
						console.log( errorThrown );
					}
				});

				if ( ! window.awear.isSingle ) {

					// jQuery( '[data-item-id="' + id + '"]' )
					// 	.parent()
					// 	.fadeIn( 'slow' );
				} else if ( window.awear.isSingle ) {

					jQuery( '.added-to-bag' ).fadeOut( 'slow', function() {
						jQuery( '.add-to-cart[data-id="' + id + '"]' ).fadeIn( 'slow' );
					});
				}

				return false;
			}
		}); // To prevent double-binding click events!
	}

	bindButtons();

	// eslint-disable-next-line no-unused-vars
	function removeButtonTest() {
		console.log( 'COME ON' );
	}

	// eslint-disable-next-line no-unused-vars
	function showCardForm() {

		//handle payment
	}

	jQuery( document ).on( 'click', '.token-payment', function() {
		jQuery( '.blocker' ).css( 'display', 'flex' );
		processPayment( 'token' );
	});

	jQuery( document ).on( 'click', '.token-delivery', function() {
		isChargingForDelivery = true;

		jQuery( '.card-payment-trigger' ).click();

		let sqMinusBtn = document.getElementById( 'sq-minus-btn' );
		let sqPlusBtn = document.getElementById( 'sq-plus-btn' );

		let payByCardButton = document.getElementById( 'pay-by-card' );
		let tokenInput = document.getElementById( 'token-input' );

		tokenInput.addEventListener( 'change', tokenAmountChange );

		payByCardButton.addEventListener( 'click', payByCard );
		payByCardButton.addEventListener( 'touchend', payByCard );

		sqMinusBtn.addEventListener( 'click', sqMinus );
		sqPlusBtn.addEventListener( 'click', sqPlus );

		sqMinusBtn.addEventListener( 'touchend', sqMinus );
		sqPlusBtn.addEventListener( 'touchend', sqPlus );

		if ( isChargingForDelivery ) {

			let deliveryRow = document.getElementById( 'delivery-row' );
			let deliveryValue = document.getElementById( 'delivery-value' );

			deliveryRow.style.display = 'flex';
			deliveryValue.setAttribute( 'data-val', 10 );

			let tempVal = 0;
			if ( 0 > jQuery( '#token-input' ).val() ) {
				tempVal = jQuery( '.top-up-value' ).attr( 'data-val' );
			}

			tempVal = parseInt( tempVal ) + 10;

			jQuery( '.top-up-value' ).html( '$' + parseInt( tempVal ) );
			jQuery( '.top-up-value' ).attr( 'data-val', parseInt( tempVal )  );

		}

	});

	jQuery( document ).on( 'click', '.card-payment', function() {

		// window.location = '/card-payment';
		jQuery( '.card-payment-trigger' ).click();

		let sqMinusBtn = document.getElementById( 'sq-minus-btn' );
		let sqPlusBtn = document.getElementById( 'sq-plus-btn' );

		let payByCardButton = document.getElementById( 'pay-by-card' );
		let tokenInput = document.getElementById( 'token-input' );

		tokenInput.addEventListener( 'change', tokenAmountChange );

		payByCardButton.addEventListener( 'click', payByCard );
		payByCardButton.addEventListener( 'touchend', payByCard );

		sqMinusBtn.addEventListener( 'click', sqMinus );
		sqPlusBtn.addEventListener( 'click', sqPlus );

		sqMinusBtn.addEventListener( 'touchend', sqMinus );
		sqPlusBtn.addEventListener( 'touchend', sqPlus );

		if ( isChargingForDelivery ) {

			let deliveryRow = document.getElementById( 'delivery-row' );
			let deliveryValue = document.getElementById( 'delivery-value' );

			deliveryRow.style.display = 'flex';
			deliveryValue.setAttribute( 'data-val', 10 );

			let tempVal = 0;
			if ( 0 > jQuery( '#token-input' ).val() ) {
				tempVal = jQuery( '.top-up-value' ).attr( 'data-val' );
			}

			tempVal = parseInt( tempVal ) + 10;

			jQuery( '.top-up-value' ).html( '$' + parseInt( tempVal ) );
			jQuery( '.top-up-value' ).attr( 'data-val', parseInt( tempVal )  );

		}

		//initialiseStripe();

	});

	jQuery( document ).on( 'click', '.process-card-payment', function() {
		processPayment( 'card' );
	});

	// eslint-disable-next-line no-unused-vars
	jQuery( document ).on( 'click', '.toggle-mobile-cart', function( e ) {
		jQuery( '.cart-inner-wrap' ).toggleClass( 'open' );
	});

	jQuery( document ).on( 'click', '.del-option', function() {

		// var date = new Date();
		let method = jQuery( this ).data( 'del' );
		localStorage.setItem( 'shippingMethod', method );
		sessionStorage.setItem( 'method', method );

		if ( window.awear.deliveryOptions.FREEDROPOFF == method ) {

			// date.setTime( date.getTime() + ( 60 * 1000 ) );

			jQuery( '.cart-shipping-row' ).fadeOut( 300 );
			jQuery( '.shipping-options' ).fadeOut( 400, function() {
				jQuery( '.pick-up-message' ).css( 'display', 'flex' );
				jQuery( '.buy-buttons' ).css( 'display', 'flex' );
			});
		} else if ( window.awear.deliveryOptions.NONMEMBERDROFF == method ) {

			isChargingForDelivery = true;

			jQuery( '.cart-shipping-row' ).find( '.shipping-total' ).html( '$10' );
			jQuery( '.shipping-options' ).fadeOut( 400, function() {
				jQuery( '.pick-up-message' ).css( 'display', 'flex' );
				jQuery( '.buy-buttons' ).css( 'display', 'flex' );
			});

		} else if ( window.awear.deliveryOptions.DELIVERY == method ) {
			alert( 'We are not quite ready to deliver outside of Queenstown. Please email us' );
		}

	});

	jQuery( document ).on( 'click', '.change-user-addy', function() {
		jQuery( '.user-addy-wrapper-text' ).fadeOut( 200, function() {
			jQuery( '.user-addy-wrapper' ).fadeIn( 300 );
		});
	});

	if ( jQuery( '.slick-wrapper' ).length ) {
		jQuery( '.slick-wrapper' ).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow:
				'<button class="slick-prev slick-arrow" aria-label="Previous" type="button"></button>',
			prevArrow:
				'<button class="slick-next slick-arrow" aria-label="Next" type="button"></button>'
		});
	}

	if ( jQuery( '.book-button' ).length ) {
		let btn = document.querySelector( '.book-button' );
		btn.addEventListener( 'click', addressButton );
		btn.addEventListener( 'touchend', addressButton );

		// eslint-disable-next-line no-inner-declarations
		function addressButton( ) {
			let dataAddress = document.querySelector( '#data-address' );
			let address = dataAddress.getAttribute( 'data-address' );
			let pickUpField = document.querySelector( '[name="pickup_address"]' );
			pickUpField.value = address;
		}
	}

	/**
	 *
	 * RELOAD AFTER ACF
	 *
	 */
	let sesh = sessionStorage.getItem( 'method' );
	if ( sesh == window.awear.deliveryOptions.FREEDROPOFF || sesh == window.awear.deliveryOptions.NONMEMBERDROFF ) {
		sessionStorage.removeItem( 'method' );

		if ( jQuery( '[data-del="free-drop-off"]' ).length ) {
			jQuery( '[data-del="free-drop-off"]' ).click();
		}

		if ( jQuery( '[data-del="non-member-drop-off"]' ).length ) {
			jQuery( '[data-del="non-member-drop-off"]' ).click();
		}

	}

	/**
	 *
	 *
	 * STRIPE
	 *
	 */

	jQuery( document ).on( 'click touchend', '.sq-button', function() {
		console.log( 'In Square Button' );
	});

	// jQuery( document ).on( 'click', '.sq-button.minus', sqMinus() );

	// jQuery( document ).on( 'click', '.sq-button.plus', sqPlus() );

}( jQuery ) );

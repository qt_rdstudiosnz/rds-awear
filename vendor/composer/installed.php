<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '6a69ca7798f574637279f9fe1f0bc1084a3f11de',
    'name' => 'richdeane/rds-awear',
  ),
  'versions' => 
  array (
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aef1bd4df0bcae158711b1701544ebbdd5049641',
    ),
    'ericmann/sessionz' => 
    array (
      'pretty_version' => '0.3.1',
      'version' => '0.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b1a278c54aa13035ed0ca0c297fb117d04036d9b',
    ),
    'ericmann/wp-session-manager' => 
    array (
      'pretty_version' => '4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5cafd9216241a53314f7e21b3888b8583f7784f',
    ),
    'giacocorsiglia/wordpress-stubs' => 
    array (
      'pretty_version' => 'v5.1.1',
      'version' => '5.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c6f011f5c241bab7293315d7fb7fca27fe79473',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cf3d8498b095bd33727b13fd5707263af99421',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'paulthewalton/acf-pro-stubs' => 
    array (
      'pretty_version' => '5.8.9.1',
      'version' => '5.8.9.1',
      'aliases' => 
      array (
      ),
      'reference' => 'b800ef8354ce4c2528b526e8c76de934620b44aa',
    ),
    'richdeane/rds-awear' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '6a69ca7798f574637279f9fe1f0bc1084a3f11de',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v7.83.0',
      'version' => '7.83.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7244c7334ad8bf30d31bb4972d5aff57df1563',
    ),
  ),
);

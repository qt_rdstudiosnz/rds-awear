<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://rdstudios.co.nz
 * @since             0.0.01
 * @package           Rds_Awear
 *
 * @wordpress-plugin
 * Plugin Name:       RDS - Awear
 * Plugin URI:        https://rdstudios.co.nz
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           0.0.46
 * Author:            Rich Deane // RD Studios
 * Author URI:        https://rdstudios.co.nz
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rds-awear
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 0.0.01 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'RDS_AWEAR_VERSION', '0.0.46' );

//session_start();

require __DIR__ . '/vendor/autoload.php';
require 'plugin-update-checker/plugin-update-checker.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/rds-awear-hooks.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/rds-awear-functions.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/rds-awear-shortcodes.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/rds-awear-schedule.php';


add_action('init',  function() {
  
  if (is_user_logged_in()) {
    
    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
     'https://bitbucket.org/qt_rdstudiosnz/rds-awear',
      __FILE__,
      'rds-awear'
    );

    $myUpdateChecker->addResultFilter(function($pluginInfo, $httpResponse = null) {

			$plugin_url = plugin_dir_url( __FILE__ );
	
			$url = rtrim($plugin_url, '/includes/');

      $pluginInfo->icons = array(
        '1x' => $url . '/assets/icon-128x128.png?rev=' . time(),
        '2x' => $url . '/assets/icon-256x256.png?rev=' . time(),
				'svg' =>  $url . '/assets/icon.svg?rev=' . time(),
      );

      return $pluginInfo;

    });

    $myUpdateChecker->setAuthentication(array(
      'consumer_key' => '67nSpwtGpfJzCfYhLG',
      'consumer_secret' => '8qR3X4CUs7b52FZQZLwFvucAUFuGNV3T',
    ));
    
    $myUpdateChecker->setBranch('release');
    
  }

});
  


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-rds-awear-activator.php
 */
function activate_rds_awear() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rds-awear-activator.php';
	Rds_Awear_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-rds-awear-deactivator.php
 */
function deactivate_rds_awear() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rds-awear-deactivator.php';
	Rds_Awear_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_rds_awear' );
register_deactivation_hook( __FILE__, 'deactivate_rds_awear' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rds-awear.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.0.01
 */
function run_rds_awear() {

	$plugin = new Rds_Awear();
	$plugin->run();

}
run_rds_awear();

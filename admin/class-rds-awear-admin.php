<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/admin
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.0.01
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.0.01
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rds_Awear_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rds_Awear_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rds-awear-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.0.01
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rds_Awear_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rds_Awear_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rds-awear-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function hide_details_n($plugin_meta, $plugin_file, $plugin_data, $status) {

		if($plugin_data['slug'] == 'rds-awear') unset($plugin_meta[2]);
		return $plugin_meta;
		
	}


}

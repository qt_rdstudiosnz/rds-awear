<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/public
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.0.01
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.0.01
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rds_Awear_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rds_Awear_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rds-awear.css', array(), $this->version . '+' . strtotime("now"), 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.0.01
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rds_Awear_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rds_Awear_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'moment', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js', array( 'jquery' ), false, false );
		$depend = array( 'jquery', 'moment' );

		if (is_singular('item')) {
			wp_enqueue_script( 'image-zoom', '//dev.awear.co.nz/wp-content/uploads/2021/04/image-zoom.js', array( 'jquery' ), false, true );
			$depend[] = 'image-zoom';
			wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), false, true );
			$depend[] = 'slick';
		}

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rds-awear-public.js', $depend, $this->version . '+' . strtotime("now"), true );

	}

	
	
}

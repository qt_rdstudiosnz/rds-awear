<?php

add_shortcode('cardPayment', function(){
  ob_start();
  //echo session_id();
  ?>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>  
  <div class="stripe-form">

    <?php
    $userBalance = getUserBalance();
    $total = getCartActual();
    $tokenRequired = ((intval($total) - intval($userBalance)) <= 0) ? 0 : intval($total) - intval($userBalance);

    $_SESSION['userBalance'] = $userBalance;
    $_SESSION['total'] = $total;
    $_SESSION['tokenRequired'] = $tokenRequired;
    ?>
    <h2>Top Up Account</h2>
    <div class="payment-table">
      <div id="payment-setup" class="payment-setup">
        <div class="payment-row token-required">
          <div>Tokens Required</div>
          <div class="tokens-required"><?php echo $tokenRequired; ?></div>
        </div>      
        <div class="payment-row add-token-row">
          <div>Tokens to Top Up</div>
          <div class="add-token-buttons">
            <div id="sq-minus-btn" class="sq-button minus " data-min="<?php echo $tokenRequired; ?>">-</div>
            <input id="token-input" type="text" data-min="<?php echo $tokenRequired; ?>" value="<?php echo $tokenRequired; ?>" class="token-input">
            <div id="sq-plus-btn" class="sq-button plus">+</div>
          </div>
        </div>      
        <div class="payment-row delivery-row" id="delivery-row" style="display: none;">
          <div>Delivery Fee</div>
          <div class="delivery-value" id="delivery-value" data-val="0">$10</div>
        </div>
        <div class="payment-row total-row">
          <div>Top Up Amount</div>
          <?php 
          $calc = isUserMember() ? get_field('m-token-val', 'option') : get_field('nm-token-val', 'option'); 
          ?>
          <div class="top-up-value" data-calc="<?php echo $calc; ?>" data-val="<?php echo convertCartToDollar($tokenRequired); ?>">$<?php echo convertCartToDollar($tokenRequired); ?></div>
        </div>
        <div class="initiator drop-shaddow" id="pay-by-card">CONFIRM TOP UP AMOUNT</div>
      </div> 
    </div>

    <div id="form-wrapper" class="form-wrapper">
      <div id="change-token-amount" class="change-token">&larr; Change Token Quantity</div>
      <div class="payment-row total-row">
        <div>Top Up Amount</div>
        <div class="top-up-value" data-val="<?php echo convertCartToDollar($tokenRequired); ?>">$<?php echo convertCartToDollar($tokenRequired); ?></div>
      </div>      
      <?php
      $user_info = get_userdata( get_current_user_id() );
      $user_email = $user_info->user_email;
      ?>
      <form id="payment-form">
        <input type="hidden" id="user_email" name="user_email" value="<?php echo $user_email; ?>" />
        <div id="card-element"><!--Stripe.js injects the Card Element--></div>
        <button id="submit">
          <div class="spinner hidden" id="spinner"></div>
          <span id="button-text">TOP UP & PAY BY CREDIT CARD NOW</span>
        </button>
      </form>

    </div>
    
    <div>
      <p id="card-error" role="alert"></p>
      <p class="result-message hidden">
        Payment Successful. Please Wait...
      </p>
    </div>

  </div>
  <?php
   $contents = ob_get_clean();
   return $contents;

});

/**
 * 
 * 
 * INSTAGRAM SHORTCODES
 * 
 * 
 */


// Makes request to Instagram and returns data in JSON format
function tct_instagram_feed_data($data)
{

  $response_data = get_field('instagram', 'option');
  return $response_data;
	  
}
// function tct_instagram_feed_data($data)
// {
//   $insta_id = 'stayingawear';

//   //if (!$response_data = get_transient('tct_instagram_feed_data' . $data)) {
//   if (!$response_data = get_field('instagram', 'option')) {

//     // If not using Wordpress then replace this section with Curl Request  
//     $request = wp_remote_get('https://www.instagram.com/' . $insta_id . '/channel/?__a=1');
//     if (is_wp_error($request)) {
//       echo 'empty'; // Bail early
//     }
// 	//	print_r($request);
	
//     $body = wp_remote_retrieve_body($request);
    
// 	//print_r($body);
//     //$response_data = json_decode($body);
//     update_field('instagram', $body, 'option');
	
// 	//set_transient('tct_instagram_feed_data' . $data, $response_data, 86400); //86400 = 1 day, 3600 = 1 hour
//   }
//   return $response_data;
// }

// Adds shortcode and sets variables from JSON data
add_shortcode('instafeed', 'tct_instagram_feed_shortcode');
function tct_instagram_feed_shortcode($atts)
{
  $data = tct_instagram_feed_data($atts['data']);
  $user = $atts['user'];
  $post_count = $atts['post_count'];
  $post_caption = $atts['caption'];
  $custom_class = $atts['class'];

  if (empty($post_count)) {
    $post_count = 12;
  }
  if ($post_caption == "true" || $post_caption == "True") {
    $post_caption = true;
  } else {
    $post_caption = false;
  }

  // To see full list of available data, check the response from this URL - https://www.instagram.com/nike/channel/?__a=1 
  $username = $data->graphql->user->username;
  $name = $data->graphql->user->full_name;
  $user_pp = $data->graphql->user->profile_pic_url;
  $user_pp_hd = $data->graphql->user->profile_pic_url_hd;
  $followers = number_format($data->graphql->user->edge_followed_by->count);
  $following = number_format($data->graphql->user->edge_follows->count);
  $bio = $data->graphql->user->biography;
  $user_post_count = $data->graphql->user->edge_owner_to_timeline_media->count;

  switch ($user) {
    case 'username':
      return $username; //string
    case 'name':
      return $name; // string
    case 'user_pp':
      return '<img src="' . $user_pp . '">'; //image
    case 'user_pp_hd':
      return '<img src="' . $user_pp_hd . '">'; // image
    case 'followers':
      return $followers; // int
    case 'following':
      return $following; // int
    case 'bio':
      return $bio; // string
    case 'user_post_count':
      return $user_post_count; // int
    case 'feed-grid':
      $feed = "";
      for ($i = 0; $i < $post_count; $i++) {
        $post_url = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->shortcode;
        $post_img = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->thumbnail_resources[3]->src;
        $post_access_caption = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->accessibility_caption;
        $post_caption_text = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->edge_media_to_caption->edges[0]->node->text;
        $post_caption_trim = tct_truncate($post_caption_text);

        if ($post_caption == true) {
          $post_caption_html = '<div class="insta-caption-div ' . $custom_class . '"><span class="insta-caption">' . $post_caption_trim . '</span></div>';
        }
        // To modify the output of the feed, change $feed string.     
        $feed .= '<a class="insta-wrapper" alt="' . $post_access_caption . '" href="https://instagram.com/p/' . $post_url . '"><img class="insta-image"  src="' . $post_img . '">' . $post_caption_html . '</a>';
      }
      return $feed;
    case 'feed-slider':
      $slider_content = "";
      // Adds Slider pre div
      $slider_pre = '<div class="insta-slider ' . $custom_class . '">';

      for ($i = 0; $i < $post_count; $i++) {
        $post_url = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->shortcode;
        $post_img = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->thumbnail_resources[1]->src;
        $post_access_caption = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->accessibility_caption;
        $post_caption_text = $data->graphql->user->edge_owner_to_timeline_media->edges[$i]->node->edge_media_to_caption->edges[0]->node->text;
        $post_caption_trim = tct_truncate($post_caption_text);


        if ($post_caption == true) {
          $post_caption_html = '<div class="slider-insta-caption-div"><span class="insta-caption">' . $post_caption_trim . '</span></div>';
        }
        // To modify the output of the feed, change $feed string.     
        $slider_content .= '<div class="slider-insta"><a class="slider-insta-wrapper" alt="' . $post_access_caption . '" href="https://instagram.com/p/' . $post_url . '"><img class="insta-image-slider"  data-lazy="' . $post_img . '">' . $post_caption_html . '</a></div>';
      }
      // Adds closing slider div	    
      $slider_after = '</div>';

      $feed_slider = $slider_pre . $slider_content . $slider_after;
      return $feed_slider;
  }
}

// Trim caption length to 100 characters
function tct_truncate($string, $length = 100, $append = "&hellip;")
{
  $string = trim($string);

  if (strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}

add_shortcode('getTheCart', 'getTheCart');
function getTheCart($atts)
{

  $atts = shortcode_atts(array(
    'checkout' => 'false',
  ), $atts, 'getTheCart');

  ob_start();
  $currentCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];

  if (!empty($currentCart)) {

    foreach ($currentCart as $item) {
  ?>
      <div class="cart-item">
        <div class="cart-thumb">
          <img alt="<?php echo $item['name']; ?>" src="<?php echo $item['thumb']; ?>" class="ct-image">
          <?php if ($atts['checkout'] == 'false') { ?>
            <div class="remove-from-bag" data-id="<?php echo $item['id']; ?>" data-slug="<?php echo $item['slug']; ?>">
              <svg viewBox="0 0 24 28">
                <path d="M19 15v-2c0-0.547-0.453-1-1-1h-12c-0.547 0-1 0.453-1 1v2c0 0.547 0.453 1 1 1h12c0.547 0 1-0.453 1-1zM24 14c0 6.625-5.375 12-12 12s-12-5.375-12-12 5.375-12 12-12 12 5.375 12 12z"></path>
              </svg>
            </div>
          <?php } ?>
        </div>
        <div class="cart-item-data">
          <div class="cart-item-title"><?php echo $item['name']; ?></div>
          <div class="cart-item-colour mb-0"><?php echo $item['colour']; ?></div>
          <div class="cart-item-colour"><?php echo $item['size']; ?></div>
          <div class="cart-item-price"><?php echo $item['price']; ?> <?php echo getCurrencyName($item['price']); ?></div>
        </div>
      </div>
    <?php
    }
  } else { ?>
    <div class="empty-message">Your bag is currently empty.</div>
<?php

  }

  $output = ob_get_clean();
  return $output;
}

add_shortcode('getShippingCost', function () {
  if (!isset($_SESSION['shipping'])) {
    return 'TO BE CALCULATED';
  } else {
    return $_SESSION['shipping'];
  }
});

add_shortcode('getCartUserBalance', function () {
  $balance = getUserBalance();
  return "<div style='display: inline-block;' data-balance='" . $balance . "' id='user-token-balance'>" . $balance . "</div>&nbsp;" . getCurrencyName($balance);
});

add_shortcode('getCartTotal', function () {
  echo getCartTotal();
});

add_shortcode('getCartConvert', function () {
  ob_start();
  $total = getCartActual();
  ?>
  <div class="dollar-only"> (<span class="or">OR </span> $<?php echo convertCartToDollar(); ?>)</div>
<?php
  $output = ob_get_clean();
  return $output;
});

add_shortcode('getCartOverallTotal', function () {
  ob_start();
  $total = getCartActual();
?>
  <div class="token-total"><?php echo $total . " " . getCurrencyName($total); ?> <?php if (shippingSet()) {
                                                                                    " and $" . shippingTotal();
                                                                                  } ?></div>
  <?php if (showCardButton()) { ?>
    <div class="dollar-only"><span class="or">&nbsp;OR&nbsp;</span>$<?php echo convertCartToDollar(); ?><?php if (shippingSet()) {
                                                                                                          " and $" . shippingTotal();
                                                                                                        } ?></div>
  <?php } ?>
<?php
  $output = ob_get_clean();
  return $output;
});


add_shortcode('getOrderDateTime', function () {
  $id = get_the_ID();
  return get_the_time('D, j F y g:i a');
});

add_shortcode('getYear', function () {
  return "Copyright ©2019 - " . date('Y') . " Awear, All Rights Reserved";
});

add_shortcode('getOrderItems', function () {

  ob_start();
  // $ids = get_field('getOrderItems');

  //var_dump($ids);
  if( have_rows('order_items') ):
    while ( have_rows('order_items') ) : the_row();
        $id = get_sub_field('id');
  ?>

    <div id="div_block-31-512" class="ct-div-block cart-item">
      <div id="div_block-32-512" class="ct-div-block cart-thumb">
        <?php $img = get_field('featured_image', $id);
        //var_dump($img);
        ?>
        <img id="image-33-512" alt="" src="<?php echo $img['sizes']['large']; ?>" class="ct-image">
      </div>
      <div id="div_block-35-512" class="ct-div-block cart-item-data">
        <div id="text_block-36-512" class="ct-text-block cart-item-title">
          <span id="span-45-512" class="ct-span"><?php echo get_the_title($id); ?></span>
        </div>
        <div class="cart-item-colour mb-0"><?php echo getColoursByID($id); ?></div>
        <div class="cart-item-colour"><?php echo getSizeByID($id); ?></div>
        <div id="text_block-38-512" class="ct-text-block cart-item-price"><?php echo get_field('price', $id) . " " . getCurrencyName(); ?></div>
      </div>
    </div>

  <?php
    endwhile;
  endif;

  $contents = ob_get_clean();
  return $contents;
});

function getPickUpMessage() {
  
  $now = strtotime("now");
  $pickUpCutOff = strtotime('saturday this week noon');

  //var_dump( $now, $pickUpCutOff );

  if ($now < $pickUpCutOff) {
      $datePickUp = date('d M Y', strtotime('sunday this week'));
      $message = 'Book a pick up for this Sunday ' . $datePickUp . ', before 6pm';
  } else {
      $datePickUp = date('d M Y', strtotime('sunday next week'));
      $message = 'Book a pick up for next Sunday ' . $datePickUp . ', before 6pm';
  }
  return $message;
  
}

add_shortcode( 'getPickUpMessage', 'getPickUpMessage' );

function getUserAddress() {
  $userID = get_current_user_id();
  $address1 = get_user_meta( $userID, 'address_1', true );
  $address2 = get_user_meta( $userID, 'address_2', true );
  $city = get_user_meta( $userID, 'city', true );
  $state = get_user_meta( $userID, 'state', true );
  $postCode = get_user_meta( $userID, 'post_code', true );
  $country = get_user_meta( $userID, 'country', true );
  $firstName = get_user_meta( $userID, 'first_name', true );
  $lastName = get_user_meta( $userID, 'last_name', true );
  
  $completeAddress = '<div id="data-address" data-address="' . $firstName . ' ' . $lastName . ', ' .
      $address1 . ', ' . 
      $address2 . ', ' .
      $city . ', ' .
      $state . ', ' .
      $postCode . ', ' .
      $country. '">' .
      $firstName . ' ' . $lastName . '<br>' .
      $address1 . '<br>' . 
      $address2 . '<br>' .
      $city . '<br>' .
      $state . '<br>' .
      $postCode . '<br>' .
      $country . '</div>';
      
  return $completeAddress;
}

add_shortcode( 'getUserAddress', 'getUserAddress' );
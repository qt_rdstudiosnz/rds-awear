<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.0.01
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.0.01
	 * @access   protected
	 * @var      Rds_Awear_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.0.01
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.0.01
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.0.01
	 */
	public function __construct() {
		if ( defined( 'RDS_AWEAR_VERSION' ) ) {
			$this->version = RDS_AWEAR_VERSION;
		} else {
			$this->version = '0.0.01';
		}
		$this->plugin_name = 'rds-awear';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rds_Awear_Loader. Orchestrates the hooks of the plugin.
	 * - Rds_Awear_i18n. Defines internationalization functionality.
	 * - Rds_Awear_Admin. Defines all hooks for the admin area.
	 * - Rds_Awear_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    0.0.01
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rds-awear-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rds-awear-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rds-awear-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rds-awear-public.php';

		$this->loader = new Rds_Awear_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rds_Awear_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    0.0.01
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Rds_Awear_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Rds_Awear_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_filter( 'plugin_row_meta', $this, 'hide_details_n', 10, 4 );
		// $this->loader->add_filter( 'pre_set_site_transient_update_plugins', $this, 'custom_icon', );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    0.0.01
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Rds_Awear_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.0.01
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     0.0.01
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     0.0.01
	 * @return    Rds_Awear_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     0.0.01
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

		
	/**
	 * Removes the View Details link from the Pkugins Page
	 * 
 	 * @since     0.0.02
	 *
	 * @param  mixed $plugin_meta
	 * @param  mixed $plugin_file
	 * @param  mixed $plugin_data
	 * @param  mixed $status
	 * @return object
	 */
	public function hide_details_n($plugin_meta, $plugin_file, $plugin_data, $status) {

		if(array_key_exists('slug', $plugin_data) && $plugin_data['slug'] == 'rds-awear') unset($plugin_meta[2]);
		return $plugin_meta;
		
	}

	public function custom_icon( $transient ) {
		if ( 
			is_object( $transient ) &&
			isset( $transient->response ) &&
			is_array( $transient->response ) 
		) {

			$basename = 'rds-awear/rds-awear.php';
			$plugin_url = plugin_dir_url( __FILE__ );
	
			// var_dump($transient);

			$url = rtrim($plugin_url, '/includes/');

			
			if ( ! isset( $transient->response[$basename] ) ) {
				return $transient;
			}

			$transient->response[$basename]->id = 'rds-awear/rds-awear.php';
			$transient->response[$basename]->icons = [
				'1x' => $url . '/assets/icon-128x128.png?rev=' . time(),
				'2x' => $url . '/assets/icon-256x256.png?rev=' . time(),
				// 'svg' =>  $url . '/assets/icon.svg?rev=' . time(),
			];

			// var_dump($url);
			// echo "<pre>";
			// print_r($transient);
			// echo "</pre>";

		}

		return $transient;
	}

}

<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.0.01
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.0.01
	 */
	public static function deactivate() {

	}

}

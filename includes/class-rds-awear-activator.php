<?php

/**
 * Fired during plugin activation
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.0.01
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.0.01
	 */
	public static function activate() {

	}

}

<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://rdstudios.co.nz
 * @since      0.0.01
 *
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.0.01
 * @package    Rds_Awear
 * @subpackage Rds_Awear/includes
 * @author     Rich Deane <rich@rdstudios.co.nz>
 */
class Rds_Awear_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.0.01
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'rds-awear',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

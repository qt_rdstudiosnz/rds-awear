<?php

if (! wp_next_scheduled ( 'resetUnavailableItems' )) {
  wp_schedule_event( time(), 'hourly', 'resetUnavailableItems' );
}

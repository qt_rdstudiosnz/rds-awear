<?php
date_default_timezone_set("Pacific/Auckland");

use MyCLabs\Enum\Enum;
class PaymentStatus extends Enum
{
    const INITIATED = 'initiated';
    const PENDING   = 'pending';
    const COMPLETED = 'completed';
    const FAILED    = 'failed';
}


class DeliveryOptions extends Enum
{
    const FREEDROPOFF   = 'free-drop-off';
    const DROPOFF   = 'drop-off';
    const DELIVERY = 'delivery';
    const NONMEMBERDROFF = 'non-member-drop-off';
}

function DeliveryOptionsText(DeliveryOptions $method) {

  $DeliveryOptionsText = array(
    DeliveryOptions::FREEDROPOFF => 'Free Drop Off',
    DeliveryOptions::DROPOFF => 'Drop Off',
    DeliveryOptions::DELIVERY => 'Delivery',
    DeliveryOptions::NONMEMBERDROFF => 'Non Member Drop Off',
  );

  return $DeliveryOptionsText[$method];

}

add_action('plugins_loaded', function(){

  if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
      'page_title'   => 'Website Settings',
      'menu_title'  => 'Website Settings',
      'menu_slug'   => 'awear-settings',
      'capability'  => 'edit_posts',
      'redirect'    => false
    ));
  }

});


add_action('init', function(){
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
});

function awearPostsStatus()
{
  register_post_status('sold', array(
    'label'                     => _x('Sold', 'item'),
    'public'                    => true,
    'exclude_from_search'       => true,
    'show_in_admin_all_list'    => true,
    'show_in_admin_status_list' => true,
    'label_count'               => _n_noop('Sold <span class="count">(%s)</span>', 'Sold <span class="count">(%s)</span>'),
  ));
}
add_action('init', 'awearPostsStatus');

// Add the custom columns to the book post type:
add_filter('manage_item_posts_columns', 'setItemColumn');
function setItemColumn($columns)
{
  unset($columns['author']);
  $columns['status'] = __('Status', 'awear');
  return $columns;
}

add_filter('manage_posts_columns', 'custom_post_columns', 999999, 2);
function custom_post_columns($columns, $post_type)
{

  switch ($post_type) {
    case 'item':
      unset(
        $columns['date'],
        $columns['seopress_title'],
        $columns['seopress_noindex'],
        $columns['seopress_desc'],
        $columns['seopress_nofollow'],
        $columns['seopress_score']
      );
      break;
  }
  return $columns;
}

// Add the data to the custom columns for the book post type:
add_action('manage_item_posts_custom_column', 'customItemColumn', 999999, 2);
function customItemColumn($column, $post_id)
{
  //var_dump($column, $post_id);
  if ($column == 'status') {

    if (get_post_status($post_id) == 'publish') {
      echo  'Listed';
    } else {
      echo ucwords(get_post_status($post_id));
    }
  }
}



add_action('admin_footer-post.php', 'wpb_append_post_status_list');
function wpb_append_post_status_list()
{
  global $post;
  $complete = '';
  $label = '';
  if ($post->post_type == 'item') {

    if ($post->post_status == 'sold') {
      $complete = 'selected="selected"';
      $label = " Sold";
    }

    $html = '
          <script>
              jQuery(document).ready(function($){
                  jQuery("select#post_status").append(\'<option value="sold" ' . $complete . '>Sold</option>\');
                  jQuery("#post-status-display").append("' . $label . '");';

    if ($post->post_status == 'sold') {
      $html .= '        jQuery("#publishing-action").replaceWith(jQuery("#save-action"));
                  jQuery("#save-post").css("float", "right").html("Save Item");';
    }

    $html .= '       });
          </script>
      ';

    echo $html;
  }
}

function isOxygenBuilder()
{
  //var_dump(preg_match('/^(?!.*ct_builder)/', $_SERVER['REQUEST_URI']));
  return preg_match('/^(?!.*ct_builder)/', $_SERVER['REQUEST_URI']);
}

function getImageURL()
{
  global $post;
  $img = get_field("featured_image", $post->ID);
  if (is_array($img)) {
    $imgUrl = $img['sizes']['large'];
  }else{
    $imgUrl = wp_get_attachment_image_src($img, 'large')[0];
  }
  return $imgUrl;
}

function getImageURLByID($ID)
{
  global $post;
  $img = get_field("featured_image", $ID);
  if (is_array($img)) {
    $imgUrl = $img['sizes']['large'];
  }else{
    $imgUrl = wp_get_attachment_image_src($img, 'large')[0];
  }
  return $imgUrl;
}

function getYear()
{
  //error_log('WE ARE IN THE GET YEAR FUNCTION');
  echo date('Y');
}

function canViewSite() {

  $maintainenceMode = get_field('maintenance_mode', 'option');

  if ( $maintainenceMode == 'on' && !isUserAdmin() ) {
    return 0;
  } else {
    return 1;
  }

}

function isUserAdmin()
{
  $user = wp_get_current_user();
  $allowed_roles = array('administrator');
  return (array_intersect($allowed_roles, $user->roles));
}

function isItemAvailable($id = 0)
{
  if ($id == 0) $id = get_the_ID();
  return get_field('item_available', $id);
}

function getCurrencyName($count = 0)
{
  $cur = get_field('currency_name', 'option');

  if ($count == 0) {
    return (get_field('price', get_the_ID()) == 1) ? $cur : $cur . "s";
  } else {
    return ($count == 1) ? $cur : $cur . "s";
  }
}

// add_action(
//   'pre_get_posts',
//   function ($query) {
//     $post_type = $query->get('post_type');

//     //if ($post_type == "page") var_dump($query);

//     if (!is_admin()) {

//       if ('item' === $post_type || 'oxygen-element-8' === $query->get('wp_grid_builder')) {
//         $query->set('post_status', 'publish');
//       }

//       if ('oxygen-element-8' === $query->get('wp_grid_builder') || ('item' === $post_type && $query->is_archive && !is_admin() && !$query->get('no_pre_post'))) {

//         $query->set('meta_key', 'item_available');
//         $query->set('meta_value', '1');
//       }
//     }
//   },
//   PHP_INT_MAX - 10
// );

function cc_mime_types($mimes)
{
  $mimes['json'] = 'application/json';
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

function rdsGetEnv($key)
{
  return getenv($key);
}

function getOrderTotal()
{
  $tokenTotal = get_field('total');

  $total = $tokenTotal . ' ' . getCurrencyName();
  return $total;
}

function getShippingTotal()
{
  return (get_field('shipping_total') == '') ? 'FREE' : get_field('shipping_total');
}

function getSize()
{
  $size = get_field('size');
  return get_term($size)->name;
}

function getSizeByID($id)
{
  $size = get_field('size', $id);
  return get_term($size)->name;
}

function getColours()
{
  $colours = get_field('colour');
  $itemColours = [];
  if (is_array($colours)) {
    foreach ($colours as $colour) {
      $itemColours[] = get_term($colour)->name;
    }
  } else {
    $colours = $itemColours;
  }
  $itemColours = (is_array($itemColours)) ? implode(' / ', $itemColours) : $itemColours;
  return $itemColours;
}

function getColoursByID($id)
{
  $colours = get_field('colour', $id);
  $itemColours = [];
  if (is_array($colours)) {
    foreach ($colours as $colour) {
      $itemColours[] = get_term($colour)->name;
    }
  } else {
    $colours = $itemColours;
  }
  $itemColours = (is_array($itemColours)) ? implode(' / ', $itemColours) : $itemColours;
  return $itemColours;
}

add_filter('fluentform_validation_user_registration_errors', 'filter_registration_errors', 999, 4);
function filter_registration_errors($errors, $formData, $form, $fields)
{

  if ($errors['restricted'][0] == "This email is already registered. Please choose another one.") {
    $errors = array('<span class="login-error-message">It looks like this email is associated to an account already. <br>Please <span class="login-now">click here to log in now</span>.</span>');
  }

  return $errors;
}

function showTokenButton()
{
  return (getUserBalance() >= getCartActual()) ? 1 : 0;
}
function showCardButton()
{
  return (getUserBalance() < getCartActual()) ? 1 : 0;
}

// function to save the randomString as custom field value
function generateAlphanumericID($postID = false, $len = 5)
{

  $characters = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";

  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i <= $len; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
  }

  //Split 
  $orderNumber = str_split($randomString, 3);
  $orderNumber = implode("-", $orderNumber);

  //Does the referral code already exist?
  /**
   * Now check here if the string is in the database
   */
  $args = array(
    'post_type' => array('orders','transactions'),
    'meta_key' => 'order-number', //any custom field name
    'meta_value' => $orderNumber //the value to compare against
  );

  $orders = new WP_Query($args);
  $results = $orders->have_posts();

  if ($results) {

    // "Match found - Redo"
    return generateAlphanumericID();
  } else {
    // "Match not found - Save To Post if Post Supplied"
    if (is_int($postID)) add_post_meta($postID, 'order-number', $orderNumber);

    return $orderNumber;
  }
}

add_action('acf/render_field/key=field_60c719fac7e2b', 'outputOrderURL');
function outputOrderURL($field)
{
  echo "<a href=" . $field['value'] . " target='_blank'>" . $field['value'] . "</a>";
}

// add_filter('acf/load_field/name=item_id', 'readOnlyField');
// add_filter('acf/load_field/name=item_code', 'readOnlyField');
// function readOnlyField($field)
// {
//   $field['disabled'] = '1';
//   return $field;
// }

add_action('acf/save_post', 'copyCode', 10);

function copyCode($post_id)
{
  $itemCode = get_post_meta($post_id, 'item_code_n', true);
  update_field('field_60bf3921dc5c8', $itemCode, $post_id);
}

add_action('save_post_item', 'generateItemCode', 10, 3);

// function to save generate a code when adding items
function generateItemCode($post_id, $post, $update)
{

  /**
   *
   * CODE STRUCURE 
   * 
   * G-T-S-NUM
   * ie: M-ACT-L-001
   * ie: F-COA-S-001
   * 
   * 
   **/

  if (!$post_id) return;
  if ($post->post_status == 'auto-draft') return;

  //var_dump($_POST);

  $currentCode = get_field('field_60bf3921dc5c8', $post_id);
  if ($update && $currentCode !== '') return;


  //error_log('WE ARE GENERATING A CODE');

  //get the tax terms for the item.
  //$size = get_the_terms( $post, 'size' );
  //$itemCategory = get_the_terms( $post, 'item_category' );
  //$gender = get_the_terms( $post, 'gender' );

  //get the codes for each term.
  //$sizeCode = get_field('category_code', 'size_' . $size->term_id);
  //$itemCategoryCode = get_field('category_code', 'item_category_' . $itemCategory->term_id);
  //$genderCode = get_field('category_code', 'gender_' . $gender->term_id);

  //$size = get_field( 'size', $post_id );$_POST['acf']['field_60bf3a4319dbf']
  $size = $_POST['acf']['field_606d0a89b404d'];
  
  //$itemCategory = get_field( 'category', $post_id );
  $itemCategory = $_POST['acf']['field_606d148a57af0'];
  
  //$gender = get_field( 'gender', $post_id );
  $gender = $_POST['acf']['field_60bf3a4319dbf'];
  
  $sizeCode = get_field('category_code', 'size_' . $size);
  $itemCategoryCode = get_field('category_code', 'item_category_' . $itemCategory);
  $genderCode = get_field('category_code', 'gender_' . $gender);


  //error_log($post_id . " - " . $size . " - " . $sizeCode);
  //error_log($post_id . " - " . $itemCategory . " - " . $itemCategoryCode);
  //error_log($post_id . " - " . $gender . " - " . $genderCode);

  //Getting the last number assigned to this code.
  $code = $genderCode . "-" . $itemCategoryCode . "-" . $sizeCode;
  $lastNumber = get_option($code);

   //Incriment the number.
  if (is_numeric($lastNumber)) {
    $number = intVal($lastNumber) + 1;
  } else {
    $number = 1;
  }
	
  //Force Number length adding Zero's at the beginning. 
  $newItemNumber = sprintf("%04d", $number);

  //error_log('STRLEN: ' . strlen($newItemNumber)) ;
 
  $newItemNumber = sprintf("%04d", $number);
  //error_log('newItemNumber: ' . $newItemNumber);

  $itemCode = $code . "-" . $newItemNumber;
  //error_log('itemCode: ' . $code . "-" . $newItemNumber);

  if (strlen($itemCode) == 12) {
    
    //Update the Option for given code and set with new number.
    update_option($code, $number);
    //error_log('Code: ' . $code);

    //Assign item code the item.
    //add_post_meta($post_id, 'item_code', $itemCode);

    $prevValue = get_post_meta($post_id, 'item_code_n', true);
    update_post_meta($post_id, 'item_code_n', $itemCode, $prevValue);

    //update_field( 'field_60bf3921dc5c8', $itemCode, $post_id );

    //return code.
    return $itemCode;

  } else {

    return;

  }

}

add_filter('acf/fields/relationship/query', 'filter_acf_relationship', 10, 3);
function filter_acf_relationship($args, $field, $post_id)
{
  $args['post_status'] = array('publish', 'sold');
  return $args;
}

add_action('admin_footer-edit.php', 'rudr_status_into_inline_edit');
function rudr_status_into_inline_edit()
{ // ultra-simple example
  echo "<script>
        jQuery(document).ready( function() {
          jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"sold\">Sold</option>' );
        });
      </script>";
}

add_action("wp_ajax_process_token_payment", "processTokenPayment");
add_action("wp_ajax_nopriv_process_token_payment", "processTokenPayment");
function processTokenPayment()
{

  $userBalance = getUserBalance();
  $total = getCartActual();

  if ($userBalance >= $total) {

    $newBalance = intval($userBalance) - intval($total);

    $userId = get_current_user_id();
    update_field('balance', $newBalance, 'user_' . $userId);

    $ids = array();

    foreach ($_SESSION['cart'] as $item) {
      $ids[] = $item['id'];
      $orderItems[] = array(
        'id' => $item['id'],
        'title' => $item['name'],
        'url' => site_url() . '/wp-admin/post.php?post=' . $item['id'] . '&action=edit'
      );
      $meta_input['item_id'] = $item['id'];
      update_field('item_available', 0, $item['id']);
      update_field('item_sold', 1, $item['id']);
      $d = new DateTime('', new DateTimeZone('Pacific/Auckland'));
      update_field('when_sold', $d->getTimestamp(), $item['id']);
    }

    global $wpdb;
    $wpdb->query("UPDATE {$wpdb->posts} SET post_status = 'sold' WHERE ID IN  (" . implode(',', $ids) . ")");

    $shippingMethod = (isset($_REQUEST['shippingMethod']) && $_REQUEST['shippingMethod'] != '') ? $_REQUEST['shippingMethod'] : DeliveryOptionsText(DeliveryOptions::FREEDROPOFF());

    $shippingTotal = $_REQUEST['shippingTotal'];

    $orderNumber = generateAlphanumericID();
    $meta_input['cart'] = json_encode($_SESSION['cart']);
    $meta_input['shipping-method'] = $shippingMethod;
    $meta_input['total'] = $total;
    $meta_input['payment-method'] = $_REQUEST['paymentMethod'];
    $meta_input['user-id'] = $userId;
    $meta_input['user'] = $userId;
    $meta_input['order-number'] = $orderNumber;
    $meta_input['items'] = $ids;
    $meta_input['shipping_total'] = $shippingTotal;

    $address1 = get_user_meta( $userId, 'address_1', true );
    $address2 = get_user_meta( $userId, 'address_2', true );
    $city = get_user_meta( $userId, 'city', true );
    $state = get_user_meta( $userId, 'state', true );
    $postCode = get_user_meta( $userId, 'post_code', true );
    $country = get_user_meta( $userId, 'country', true );
    $firstName = get_user_meta( $userId, 'first_name', true );
    $lastName = get_user_meta( $userId, 'last_name', true );

    $meta_input['drop_of_address'] = $firstName . ' ' . $lastName . ', ' . $address1 . ', ' . $address2 . ', ' . $city . ', ' . $state . ', ' . $postCode . ', ' . $country;

    if (showThisSundayMessage()) {
      $meta_input['drop-date'] = date('d M Y', strtotime('this Sunday'));
      $meta_input['drop_off_date'] = date('d M Y', strtotime('this Sunday'));
    }else{
      $meta_input['drop-date'] = date('d M Y', strtotime('next Sunday'));
      $meta_input['drop_off_date'] = date('d M Y', strtotime('next Sunday'));
    }

    $shipping = $shippingTotal;
    // $meta_input['shipping_total'] = $shipping . '.00';
    $meta_input['order_dollar_total'] = $shipping . '.00';

    $meta_input['item_code'] = get_field('item_code', $item['id']);

    $pn = wp_generate_uuid4();

      $arr = array(
      'post_type' => 'orders',
      'post_status'  => 'publish',
      'post_name' => $pn,
      'post_title' => $orderNumber,
      'tax_input' => array(
        'order_type' => 'purchase'
      ),
      'meta_input' => $meta_input
    );

    $newPostID = wp_insert_post($arr);

    update_field('items', $ids, $newPostID);
    update_field('order_items', $orderItems, $newPostID);

    $message = array(
      "success" => true,
      "id" => $newPostID,
      "permalink" => get_permalink($newPostID)
    );

    sendAdminEmail($newPostID);
    sendCustomerEmail($newPostID);

    destryAllSessions();

    wp_send_json_success(json_encode($message), 200);
    die();
  } else {
    $error = "You do not have sufficient " . getCurrencyName(2) . ". Please top up and try again.";

    $message = array(
      "success" => false,
      "permalink" => "/check-out?error=" . urlencode($error),
      "error" => $error
    );

    wp_send_json_success(json_encode($message), 200);
    die();
  }
}

function sendAdminEmail($post_id) {

  $post = get_post($post_id);
	
	$orderNumber = get_the_title($post_id);

  $order_items = get_field('order_items', $post_id);
	$dropDate = get_post_meta($post_id, 'drop-date', true );

  $userID = get_post_meta($post_id, 'user-id', true);
  $price = get_post_meta($post_id, 'price', true);

  $drop_off_date = get_post_meta($post_id, 'drop_off_date', true);
  $drop_of_address = get_post_meta($post_id, 'drop_of_address', true);
	//$user = get_user_by('id', 1);

  // $address1 = get_user_meta( $userID, 'address_1', true );
  // $address2 = get_user_meta( $userID, 'address_2', true );
  // $city = get_user_meta( $userID, 'city', true );
  // $state = get_user_meta( $userID, 'state', true );
  // $postCode = get_user_meta( $userID, 'post_code', true );
  // $country = get_user_meta( $userID, 'country', true );
  
  $firstName = get_user_meta( $userID, 'first_name', true );
  $lastName = get_user_meta( $userID, 'last_name', true );

  $names = array(
		$firstName,
		$lastName
	);

  $items = '<table>';

  if( have_rows('order_items', $post_id) ):
    while ( have_rows('order_items', $post_id) ) : the_row();
      $id = get_sub_field('id');
      $imageURL = getImageURLByID($id);
      $itemCode = get_field('item_code', $id);
      $title = get_sub_field('title');
      $items .= '<tr><td><img src="' . $imageURL . '" style="width: 100px; height: auto" /></td><td>' . $title . '<br><small>(' . $itemCode . ')</small></td></tr>';

        // Do something...
    endwhile;
  endif;
  
  $items .= '</table>';

	$adminHeaders[] = 'From: Awear <info@awear.co.nz>';
	// $adminHeaders[] = 'CC: Bar Hero Bookings <bookings@barhero.co.nz>';

	$adminContent = '<h2>Awesome - An Order has been placed!</h2>';
  $adminContent .= '<br>';
	$adminContent .= '<strong>Order Number: </strong> ' . $orderNumber;
  $adminContent .= '<br>';
	$adminContent .= '<strong>User: </strong> ' . $firstName . ' ' . $lastName . ' (ID: )' . $userID;
	$adminContent .= '<br>';
	$adminContent .= '<br>';
	$adminContent .= '<strong>Items: </strong>  <br>' . $items;
	$adminContent .= '<br>';
	$adminContent .= '<br>';  
	$adminContent .= '<strong>Drop Off Address:</strong>  ' . $drop_of_address;
	$adminContent .= '<br>';
	$adminContent .= '<strong>Drop Off Date:</strong>  ' . $drop_off_date;
	$adminContent .= '<br>';
	$adminContent .= '<strong>Price:</strong>  ' . $price;


	$adminHeaders[] = 'Content-Type: text/html; charset=UTF-8';
	wp_mail('sarah@awear.co.nz', 'Order Placed: ' . $orderNumber, $adminContent, $adminHeaders);
	return true;


}

function sendCustomerEmail($post_id) {
  
  $post = get_post($post_id);
	
	$orderNumber = get_the_title($post_id);

  $order_items = get_field('order_items', $post_id);
	$dropDate = get_post_meta($post_id, 'drop-date', true );
  
  $userID = get_post_meta($post_id, 'user-id', true);
  $user = get_userdata($userID);

  $email = $user->user_email;

  $price = get_post_meta($post_id, 'price', true);
	//$user = get_user_by('id', 1);

  $drop_off_date = get_post_meta($post_id, 'drop_off_date', true);
  $drop_of_address = get_post_meta($post_id, 'drop_of_address', true);

  $firstName = get_user_meta( $userID, 'first_name', true );
  $lastName = get_user_meta( $userID, 'last_name', true );

  $names = array(
		$firstName,
		$lastName
	);

  $items = '<table>';

  if( have_rows('order_items', $post_id) ):
    while ( have_rows('order_items', $post_id) ) : the_row();
      $id = get_sub_field('id');
      $imageURL = getImageURLByID($id);
      $itemCode = get_field('item_code', $id);
      $title = get_sub_field('title');
      $items .= '<tr><td><img src="' . $imageURL . '" style="width: 100px; height: auto" /></td><td>' . $title . '</td></tr>';

        // Do something...
    endwhile;
  endif;

  $items .= '</table>';

	$userHeaders[] = 'From: Awear <info@awear.co.nz>';
	// $userHeaders[] = 'CC: Bar Hero Bookings <bookings@barhero.co.nz>';

	$userContent = '<h2>Awesome - Your Order has been placed!</h2>';
  $userContent .= '<br>';
	$userContent .= '<strong>Order Number: </strong> ' . $orderNumber;
  $userContent .= '<br>';
	$userContent .= '<strong>User: </strong> ' . $firstName . ' ' . $lastName;
	$userContent .= '<br>';
	$userContent .= '<br>';
	$userContent .= '<strong>Items: </strong>  <br>' . $items;
	$userContent .= '<br>';
	$userContent .= '<br>';  
	$userContent .= '<strong>Drop Off Address:</strong>  ' . $drop_of_address;
	$userContent .= '<br>';
	$userContent .= '<strong>Drop Off Date:</strong>  ' . $drop_off_date;
	$userContent .= '<br>';
	$userContent .= '<strong>Price:</strong>  ' . $price;


	$userHeaders[] = 'Content-Type: text/html; charset=UTF-8';
	wp_mail($email, 'Order Placed: ' . $orderNumber, $userContent, $userHeaders);
	return true;


}


function destryAllSessions()
{
  session_start();

  // Unset all of the session variables.
  $_SESSION = array();

  // If it's desired to kill the session, also delete the session cookie.
  // Note: This will destroy the session, and not just the session data!
  if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(
      session_name(),
      '',
      time() - 42000,
      $params["path"],
      $params["domain"],
      $params["secure"],
      $params["httponly"]
    );
  }

  // Finally, destroy the session.
  session_destroy();
  return true;
}

add_action("wp_ajax_setTopUpValue", "setTopUpValue");
add_action("wp_ajax_nopriv_setTopUpValue", "setTopUpValue");

function setTopUpValue()
{

  $topUpVal = $_REQUEST['topUpVal'];
  unset($_SESSION['paymentVal']);
  $_SESSION['paymentVal'] = $topUpVal;
  
  $message = "Payment Value Set:" . $topUpVal . " | Payment Value Session Val:" . $_SESSION['paymentVal'];

  wp_send_json_success($message, 210);
  die();
}



add_action("wp_ajax_destroySession", "destroySession");
add_action("wp_ajax_nopriv_destroySession", "destroySession");

function destroySession()
{

  destryAllSessions();

  $message = "Session destroyed.";
  wp_send_json_success($message, 210);
  die();
}

add_action("wp_ajax_add_to_cart", "add_to_cart");
add_action("wp_ajax_nopriv_add_to_cart", "add_to_cart");

function add_to_cart()
{
  //session_start();

  if (!wp_verify_nonce($_REQUEST['nonce'], "add_to_cart")) {
    exit("No naughty business please");
  }

  $isItemAvailable = get_field('item_available', $_REQUEST['id']);

  if ($isItemAvailable) {

    $currentCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];

    $item = array(
      'id' => $_REQUEST['id'],
      'name' => $_REQUEST['name'],
      'slug' => $_REQUEST['slug'],
      'thumb' => $_REQUEST['thumb'],
      'price' => $_REQUEST['price'],
      'size' => $_REQUEST['size'],
      'colour' => $_REQUEST['colour'],
    );

    update_field('item_available', 0, $item['id']);

    $d = new DateTime('', new DateTimeZone('Pacific/Auckland'));
    $d->add(new DateInterval('PT30M'));
    update_field('added_to_cart', $d->getTimestamp(), $item['id']);
    //update_field('dummy', $d->getTimestamp(), $item['id']);

    //update_field('dummy', date_default_timezone_get(), $item['id']);

    $currentCart[] = $item;
    $_SESSION['cart'] = $currentCart;
    wp_send_json_success(json_encode($currentCart), 200);
  } else {
    $message = "Sorry, someone just beat you to it. Please try again later or pick something else in our inventory.";

    wp_send_json_success($message, 210);
  }
}

add_action("wp_ajax_remove_from_cart", "remove_from_cart");
add_action("wp_ajax_nopriv_remove_from_cart", "remove_from_cart");

function remove_from_cart()
{

  //if ( !wp_verify_nonce( $_REQUEST['nonce'], "add_to_cart") ) {
  //    exit("No naughty business please");
  //}

  $currentCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];

  $id = $_REQUEST['id'];

  foreach ($currentCart as $key => $val) {
    if ($val["id"] == $id) {
      unset($currentCart[$key]);
    }
  }

  update_field('item_available', 1, $id);
  update_field('added_to_cart', "", $id);
  //update_field('dummy', "", $id);

  $_SESSION['cart'] = $currentCart;
  wp_send_json_success(json_encode($currentCart), 200);
}

function setAwearNonce($action)
{
  $nonce = wp_create_nonce($action);
  return $nonce;
}

function getCartActual()
{
  $currentCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];
  $total = 0;
  foreach ($currentCart as $item) {
    $total += $item['price'];
  }
  return $total;
}

function shippingSet()
{
  return isset($_SESSION['shipping']);
}

function shippingTotal()
{
  return $_SESSION['shipping'];
}

function isLoggedInUserMember()
{
  if (class_exists('MeprUser')) {
    $user_id = get_current_user_id();
    $mepr_user = new MeprUser( $user_id );
    if( $mepr_user->is_active() ) {
      return 1;
    } else if( $mepr_user->has_expired() ) {
      return 0;
    }else {
      return 0;
    }
  }else{
    return 0;
  }

}

function isUserMember()
{
  if (class_exists('MeprUser')) {
    $user_id = get_current_user_id();
    $mepr_user = new MeprUser( $user_id );
    if( $mepr_user->is_active() ) {
      return true;
    } else if( $mepr_user->has_expired() ) {
      return false;
    }else {
      return false;
    }
  }else{
    return false;
  }

}

function convertCartToDollar( $val = 0 )
{
  $nmRate = get_field('nm-token-val', 'option');
  $mRate = get_field('m-token-val', 'option');
  $total = ($val === 0) ? getCartActual() : intval($val);
  $conRate = (isUserMember()) ? $mRate : $nmRate;

  return intval($conRate) * intval($total);
}

function getUserBalance()
{
  $id = get_current_user_id();
  $balance = get_field('balance', 'user_' . $id);
  return intval($balance);
}

function getCartTotal()
{

  ob_start();
  $total = getCartActual();

?>
  <div id="text_block-150-50" class="ct-text-block cart-total"><?php echo $total; ?></div>
  <div id="text_block-153-50" class="ct-text-block cart-token"><?php echo getCurrencyName($total); ?></div>
  <?php

  $output = ob_get_clean();
  return $output;
}



add_action('resetUnavailableItems', function () {

  $d = new DateTime('', new DateTimeZone('Pacific/Auckland'));
  $d->add(new DateInterval('PT60M'));
  $timestamp = $d->getTimestamp();

  $args = array(
    'no_pre_post' => 1,
    'post_type' => 'item',
    'posts_per_page' => -1,
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'item_available',
        'value'   => 0,
        'compare' => '=='
      ),
      array(
        'key'     => 'added_to_cart',
        'value'   => $timestamp,
        'compare' => '<'
      ),
      array(
        'relation' => 'OR',
        array(
          'key'     => 'item_sold',
          'value'   => 0,
          'compare' => '=='
        ),
        array(
          'key'     => 'item_sold',
          'compare' => 'NOT EXISTS'
        ),
      ),
    ),
  );

  $items = new WP_Query($args);

  $htmlItems = "";
  $plainItems = "";
  foreach ($items->posts as $item) :

    update_field('item_available', 1, $item->ID);
    update_field('added_to_cart', "", $item->ID);

    $htmlItems .= $item->ID . " Reset<br>";
    $plainItems .=  $item->ID . " Reset \n\r";
  endforeach;

  $message = "These Items were Reset <br><br>\n\n";
  $message .= $htmlItems;
  $message .= $plainItems;

  wp_mail("rich@richdeane.co.nz", "AWEAR CRON RAN", $htmlItems);
});

add_action("wp_ajax_updateUserBalance", "updateUserBalance");
add_action("wp_ajax_nopriv_updateUserBalance", "updateUserBalance");

function updateUserBalance() {
  
  //error_log(getSessionCartTotal());
  header('Content-Type: application/json');

  try {
    $awearUser = $_POST['awearUser'];
    $addedTokens = $_POST['addedTokens'];

    $oldVal = get_field( 'balance', 'user_' . $awearUser ); 

    $newVal = intval($oldVal) + intval($addedTokens);

    update_field( 'balance', $newVal, 'user_' . $awearUser ); 

    $output = [
      'code' => '200',
    ];
    
    echo json_encode($output);
    die();

  } catch (Error $e) {

    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
    die();

  }

} 


add_action("wp_ajax_payment_intent", "paymentIntent");
add_action("wp_ajax_nopriv_payment_intent", "paymentIntent");

function paymentIntent() {

  $key = getenv('STRIPE_DEV_SECRET');
  $stripe = new \Stripe\StripeClient(
    $key
  );

  //error_log(getSessionCartTotal());
  header('Content-Type: application/json');

  try {

    // // retrieve JSON from POST body
    // $json_str = file_get_contents('php://input');
    // $json_obj = json_decode($json_str, true);

    // $paymentVal = (isset($_SESSION['paymentVal']) && $_SESSION['paymentVal'] > 1) ? $_SESSION['paymentVal'] : getSessionCartTotal();
    // $paymentVal = (isset($_SESSION['paymentVal']) && $_SESSION['paymentVal'] > 1) ? $_SESSION['paymentVal'] : $_SESSION['sessionCartTotal'];
    
    $isAwearArmy = $_POST['isAwearArmy'];
    
    if (isset($_POST['deliveryCharge'])) $isDeliveryCharge = intVal($_POST['deliveryCharge']);

    $nmRate = get_field('nm-token-val', 'option');
    $mRate = get_field('m-token-val', 'option');

    $isAwearArmy = ($isAwearArmy === 'true') ? true : false;

    $rate = ($isAwearArmy) ? intval($mRate) : intval($nmRate);
    $paymentVal = intval($_POST['tokenCount']) * $rate;
    
    $shipping = 10;

    if ($isDeliveryCharge == 1) {
      $paymentVal = intVal($shipping) + intVal($paymentVal);
      //$order_dollar_total = intVal($shipping) + intVal($paymentVal);
    }

    error_log('paymentVal: ' . $paymentVal);

    $description = (isset($_REQUEST['description'])) ? $_REQUEST['description'] : 'Account Top Up';

    $paymentIntent = $stripe->paymentIntents->create([
      'amount' => $paymentVal . '00',
      'currency' => 'nzd',
      'description' => $description
    ]);

    $pn = wp_generate_uuid4();
    $orderNumber = generateAlphanumericID();
    $userId = $_POST['awearUser'];

    $meta_input['total_dollar_paid'] = $paymentVal . '.00';
    $meta_input['shipping_total'] = $shipping . '.00';
    $meta_input['order_dollar_total'] = $paymentVal . '.00';
    $meta_input['payment-method'] = 'Credit Card';
    $meta_input['user-id'] = $userId;
    $meta_input['user'] = $userId;
    $meta_input['order-number'] = $orderNumber;
    $meta_input['stripe-id'] = $paymentIntent->id;
    $meta_input['cart'] = json_encode($paymentIntent);

    $arr = array(
      'post_type' => 'transactions',
      'post_status'  => 'publish',
      'post_name' => $pn,
      'post_title' => $orderNumber,
      'tax_input' => array(
        'order_type' => 'top-up'
      ),
      'meta_input' => $meta_input
    );

    $newPostID = wp_insert_post($arr);
    $output = [
      'code' => '200',
      'clientSecret' => $paymentIntent->client_secret,
      'id' => $paymentIntent->id,
      'rate' => $rate,
      'isAwearArmy' => $isAwearArmy,
      "success" => true,
      "shippingTotal" => $shipping,
      "isChargingDelivery" => $isDeliveryCharge,
      "id" => $newPostID,
      "permalink" => get_permalink($newPostID),
    ];

    echo json_encode($output);
    die();
  } catch (Error $e) {
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
    die();
  }

}

function getSessionCartTotal() {

  $total = 0;
  foreach ($_SESSION['cart'] as $item) {
    $total += intval($item['price']);
  }

  $nmRate = get_field('nm-token-val', 'option');
  $mRate = get_field('m-token-val', 'option');
  $conRate = (isUserMember()) ? $mRate : $nmRate;

  $_SESSION['sessionCartTotal'] = intval($conRate) * intval($total);
  return intval($conRate) * intval($total);

}

function isCartEmpty() {
  return (isset($_SESSION['cart'])) ? 0 : 1;
}

function isUserLoggedIn() {
  return (is_user_logged_in()) ? 1 : 0;
}


function showGeneralMessage() {
  return 0;
}

function showThisSundayMessage() {
  $now = strtotime("now");
  $fridayNoon = strtotime('friday this week noon');
  return ($now < $fridayNoon);
}

function showNextSundayMessage() {
  $now = strtotime("now");
  $fridayNoon = strtotime('friday this week noon');
  return ($now > $fridayNoon);
}

function isInAddressTab() {
  $action = $_GET('action');
  return ( $action == "address" );
}


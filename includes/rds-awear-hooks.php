<?php

add_action('wp_head', function () {
?>
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#a66254">
  <meta name="msapplication-TileColor" content="#a66254">
  <meta name="theme-color" content="#a66254">
<?php
});


add_action('save_post', 'generateGuidOnSave');

function generateGuidOnSave($post_ID, $post = null, $update = false)
{
  //	error_log(get_post_type( $post_ID ) . PHP_EOL, 0);
  // if ( (get_post_type( $post_ID ) == 'item')  -->&& !$update) {
  if ((get_post_type($post_ID) == 'item') && !$update || (get_post_type($post_ID) == 'orders') && !$update) {

    $cur = get_field('item_id', $post_ID);
    error_log($cur);
    if ($cur == '') {

      $item_id = wp_generate_uuid4();
      //error_log($item_id . PHP_EOL, 0);

      //update_post_meta($post_ID, 'item_id', $booking_id, '');
      if (get_post_type($post_ID) == 'item') update_field('item_id', $item_id, $post_ID);

      if (!wp_is_post_revision($post_ID)) {

        // unhook this function to prevent infinite looping
        remove_action('save_post', 'generateGuidOnSave');

        // update the post slug
        wp_update_post(array(
          'ID' => $post_ID,
          'post_name' => $item_id // do your thing here
        ));

        // re-hook this function
        add_action('save_post', 'generateGuidOnSave');
      }
    }
  }
}


add_filter('get_the_archive_title', 'my_theme_archive_title');
/**
 * Remove archive labels.
 * 
 * @param  string $title Current archive title to be displayed.
 * @return string        Modified archive title to be displayed.
 */
function my_theme_archive_title($title)
{
  if (is_category()) {
    $title = single_cat_title('', false);
  } elseif (is_tag()) {
    $title = single_tag_title('', false);
  } elseif (is_author()) {
    $title = '<span class="vcard">' . get_the_author() . '</span>';
  } elseif (is_post_type_archive()) {
    $title = post_type_archive_title('', false);
  } elseif (is_tax()) {
    $title = single_term_title('', false);
  }

  return $title;
}


// add_action(
//   'pre_get_posts',
//   function ($query) {
//     $post_type = $query->get('post_type');
//     //var_dump($post_type);

//     if ('oxygen-element-8' === $query->get('wp_grid_builder') || 'oxygen-element-28' === $query->get('wp_grid_builder')) {
//       $query->set('meta_key', 'item_available');
//       $query->set('meta_value', '1');
//     }
//   },
//   PHP_INT_MAX - 10
// );

add_action('wp_head', function () {
  $currentCart = ((session_id() != '' || isset($_SESSION)) && isset($_SESSION['cart'])) ? $_SESSION['cart'] : [];

  $nmRate = get_field('nm-token-val', 'option');
  $mRate = get_field('m-token-val', 'option');
  
  $rate = isUserMember() ? $mRate : $nmRate;
  $userID = get_current_user_id();

  $pat = array(
    'ajax_url' => admin_url('admin-ajax.php'),
    'isSingle' => is_single(),
    'currencyName' => get_field('currency_name', 'option'),
    'currentCart' => json_encode($currentCart),
    'isAwearArmy' => isUserMember(),
    'tokenRate' => $rate,
    'awearUser' => $userID,
    'deliveryOptions' => array('FREEDROPOFF' => DeliveryOptions::FREEDROPOFF, 'DELIVERY' => DeliveryOptions::DELIVERY, 'DROPOFF' => DeliveryOptions::DROPOFF, 'NONMEMBERDROFF' => DeliveryOptions::NONMEMBERDROFF),
  );

  if (is_page(
    array('card-payment', 'check-out')
  )) {
    $pat['pk'] = getenv('STRIPE_DEV_PUBLISH');
  }
?>
  <script>
    var awear = <?php echo json_encode($pat); ?>
  </script>
<?php
});


add_filter('fluentform_validate_input_item_input_password', function ($errorMessage, $field, $formData, $fields, $form) {

  error_log('We are validating Password');

  $target_form_id = 3;
  if ($form->id != $target_form_id) {
    return $errorMessage;
  }

  $password = $formData['password'];
  $password_confirm = $formData['password_confirm'];

  $errorMessage = '';

  // 12 charcter min
  $pattern = "/(?=.{8,})/";
  $match = preg_match($pattern, $password);

  //preg_match($pattern, $password, $matches, PREG_OFFSET_CAPTURE, 0);
  if (!$match) {

    $errorMessage = "You need minimum 8 of charcters!";
    return [$errorMessage];
  }

  if ($formData['password'] !=  $formData['password_confirm']) {
    $errorMessage =  ['Sorry, your passwords do not match.'];
    return [$errorMessage];
  }

  return;
}, 10, 5);


add_action('template_redirect', 'redirectHandler');

function redirectHandler()
{
  if (!is_admin()) {

    $userID = get_current_user_id();
    $queried_post_type = get_query_var('post_type');
    $pID = get_the_ID();

    if ('orders' ==  $queried_post_type) {
      $orderUserID = get_post_meta($pID, 'user-id', true);

      //var_dump(is_single(), intVal($orderUserID), intVal($id), is_archive(), !isUserAdmin());

      if ((is_single() && intVal($orderUserID) != intVal($userID)) ||
        (is_archive() && !isUserAdmin())
      ) {
        //wp_redirect( '/swap', 301 );
        //exit;
      }
    }
  }
}

add_action( 'template_redirect', 'redirect_if_user_not_logged_in' );
function redirect_if_user_not_logged_in() {

  if ( ( is_post_type_archive('orders') && ! isUserAdmin() ) || ( is_singular('orders') && ! is_user_logged_in() ) ) {
    $url = site_url();
    wp_redirect( $url );
    exit;
  }
}

/**
 * 
 * Account Functions
 *
 * @param [type] $user
 * @return void
 */
function mepr_add_some_tabs($user) {
?>

  <span class="mepr-nav-item account-balance <?php MeprAccountHelper::active_nav('account-balance'); ?>">
    <a href="/account/?action=account-balance">Account Balance</a>
  </span>
  
  <span class="mepr-nav-item address <?php MeprAccountHelper::active_nav('address'); ?>">
    <a href="/account/?action=address">User Address</a>
  </span>
  
  <span class="mepr-nav-item book-pick-up <?php MeprAccountHelper::active_nav('book-pick-up'); ?>">
    <a href="/account/?action=book-pick-up">Book A Pick Up</a>
  </span>
  
  <span class="mepr-nav-item orders <?php MeprAccountHelper::active_nav('past'); ?>">
    <a href="/account/?action=past">Past Orders</a>
  </span>

  <?php 
  /*
  <span class="mepr-nav-item top-ups <?php MeprAccountHelper::active_nav('top-ups'); ?>">
    <a href="/account/?action=top-ups">Top Ups</a>
  </span> 
<?php
*/
}
add_action('mepr_account_nav', 'mepr_add_some_tabs');

function mepr_add_tabs_content($action) {
//Listens for the "premium-support" action on the account page, before rendering the contact form shortcode.
if($action == 'past') {
    ?>
    
      <?php 
      $args = array(
          'post_type' => 'orders',
          'posts_per_page' => -1,
          'meta_query' => array(
              array(
                  'key'     => 'user-id',
                  'value'   => get_current_user_id(),
                  'compare' => '=',
              ),
          ),
          'tax_query' => array(
              array(
                  'taxonomy' => 'order_type',
                  'field'    => 'slug',
                  'terms'    => 'purchase',
              ),
          ),
      );
      $orders = new WP_Query($args);
  
      if ($orders->found_posts) :
          ?>
          <table class="order-table w-100">
              <tr class="table-header-row">
                <th class="order-name">Order ID</th>
                <th class="token-total">Token Total</th>
                <th class="order-total">Total</th>
                <th class="order-date">Date</th>          
                <th class="order-links"></th>          
              </tr>
          <?php
          foreach ($orders->posts as $orderPost) :
              //var_dump($orderPost);
              ?>
              <tr class="table-row">
                <td class="order-name"><?php echo $orderPost->post_title; ?></td>
                <td class="token-total"><?php echo get_post_meta($orderPost->ID, 'total', true); ?></td>
                <td class="order-total"><?php echo get_post_meta($orderPost->ID, 'total_dollar_paid', true); ?></td>
                <td class="order-date"><?php echo get_the_date('j F Y', $orderPost); ?></td>          
                <td class="order-links"><a href="<?php the_permalink($orderPost); ?>" target="_blank" >View Order</a></td>          
              </tr>
          <?php endforeach; ?>
            </table>
      <?php else: ?>
                  <tr class="table-row">
                      <td colspan="5">No Past Orders</td>
                  </tr>
              </table>
          <?php
      endif;
      
}else if($action == 'account-balance') {
  
  $balance = getUserBalance();

  echo "<div class='acccount'>";
  echo "<div class='account-wrapper'><h2>Your Account Balance</h2></div>";
  echo "<div class='account-wrapper'><div style='display: inline-block;' data-balance='" . $balance . "' id='user-token-balance'>" . $balance . "</div>&nbsp;" . getCurrencyName($balance) . "</div>";
  echo "</div>";

}else if($action == 'book-pick-up') {
    
    echo '<div class="user-addy-wrapper">' . do_shortcode('[fluentform id="6"]') . '</div>';
    
}else if($action == 'top-ups') {
    ?>
    Top Ups
    <?php
}else if ($action == 'address') {
    
    echo '<div class="user-addy-wrapper">' . do_shortcode('[acfe_form name="update-users-form"]') . '</div>';
}
}
add_action('mepr_account_nav_content', 'mepr_add_tabs_content');


add_filter( 'posts_search', 'awear_item_search' );

function awear_item_search( $where ) {
    global $pagenow, $wpdb, $wp;

    // check if we are on the right page & performing a search & for the right post type
    if ( 'edit.php' != $pagenow || ! is_search() || ! isset( $wp->query_vars['s'] ) || 'item' != $wp->query_vars['post_type'] ) {
        return $where;
    }
    
    // the meta key of the custom field we are looking for
	$meta_key = 'item_code';

	$search_ids = array();
	$terms      = explode( ',', $wp->query_vars['s'] );


	foreach ( $terms as $term ) {
        if ( is_numeric( $term ) ) {
            $search_ids[] = $term;
        }

        // Attempt to get a VIN
        $code_to_id = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_parent FROM {$wpdb->posts} LEFT JOIN {$wpdb->postmeta} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id WHERE meta_key='{$meta_key}' AND meta_value LIKE %s;", '%' . $wpdb->esc_like( $term ) . '%' ) );
        $code_to_id = array_merge( wp_list_pluck( $code_to_id, 'ID' ), wp_list_pluck( $code_to_id, 'post_parent' ) );

        // include the ids of relevant posts in the results
        if ( sizeof( $code_to_id ) > 0 ) {
            $search_ids = array_merge( $search_ids, $code_to_id );
        }
    }

    $search_ids = array_filter( array_unique( array_map( 'absint', $search_ids ) ) );

    if ( sizeof( $search_ids ) > 0 ) {
        // if ids found, change the where clause
        $where = str_replace( 'AND (((', "AND ( ({$wpdb->posts}.ID IN (" . implode( ',', $search_ids ) . ")) OR ((", $where );
    }

    return $where;
}

=== Plugin Name ===
Contributors: richdeane
Donate link: https://rdstudios.co.nz
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 0.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==
This is the Awear Site Plugin for all website functions.

== Changelog ==

= 0.0.46 =
* Updating CC Charge For Delivery

= 0.0.45 =
* Adding Delivery Charge To Check Out

= 0.0.44 =
* Updates

= 0.0.43 =
* JS Issues

= 0.0.42 =
* Adding Paid Delivery

= 0.0.41 =
* JS Fix for Fail to add cart

= 0.0.40 =
* Version Bump

= 0.0.39 =
* Fix Address Addition & Blocker

= 0.0.38 =
* Maintenance Mode

= 0.0.37 =
* Adding Transaction Post Type & Features

= 0.0.36 =
* Prevent Click When Scroll

= 0.0.35 =
* Quick Fix

= 0.0.34 =
* Fixing Issues

= 0.0.33 =
* Fixing Order Emails

= 0.0.32 =
* Adding Order Emails

= 0.0.31 =
* Pushing To Live Site

= 0.0.30 =
* Some Updates

= 0.0.29 =
* Fixing Item Code 

= 0.0.28 =
* Adding ENUM For Delivery Options

= 0.0.27 =
* Adding Top Up Tracking

= 0.0.26 =
* Updating Checkout

= 0.0.25 =
* Adding Memberpress Interaction

= 0.0.24 =
* Adding Memberpress Interaction

= 0.0.23 =
* Fixing Check out & credit card top up functionality

= 0.0.21 =
* CC Fix

= 0.0.20 =
* Fixer

= 0.0.19 =
* Fixer

= 0.0.18 =
* Updates

= 0.0.17 =
* Stripe Updates

= 0.0.16 =
* Fixing Errors

= 0.0.15 =
* Removing all dependancies on Advanced Scripts

= 0.0.14 =
* Hooking Up Composer

= 0.0.13 =
* Adding Session Management

= 0.0.11 =
* Adding Code from

= 0.0.09 =
* Adding Plugin Icon

= 0.0.08 =
* Getting Updates Going

= 0.0.06 =
* Getting Updates Going

= 0.0.01 =
* Initial Launch.

== Upgrade Notice ==
= 0.0.12 =
* Adding Code from Advanced Scripts
= 0.0.09 =
* Adding Plugin Icon
= 0.0.08 =
* Getting Updates Going
= 0.0.07 =
Still trying to get remote updates working
= 0.0.06 =
Adding Vital Dev